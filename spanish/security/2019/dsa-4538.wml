#use wml::debian::translation-check translation="e41f5efa353b2bdd34609a011c02c9132873e041"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se encontraron dos vulnerabilidades en la implementación del protocolo WPA presente en
wpa_supplication (estación) y en hostapd (punto de acceso).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13377">CVE-2019-13377</a>

    <p>Un atacante podría realizar un ataque de canal lateral basado en tiempo contra
    el «handshake» Dragonfly de WPA3 cuando se usan curvas Brainpool, para conseguir la
    contraseña.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16275">CVE-2019-16275</a>

    <p>Una validación insuficiente de direcciones de origen para algunas tramas de gestión recibidas
    en hostapd podría dar lugar a denegación de servicio para estaciones asociadas a un
    punto de acceso. Un atacante dentro del radio de alcance del punto de acceso podría inyectar en
    este una trama IEEE 802.11 no autenticada especialmente construida
    para provocar que las estaciones asociadas sean desconectadas y precisen reconectarse
    a la red.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2:2.7+git20190128+0c1e29f-6+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de wpa.</p>

<p>Para información detallada sobre el estado de seguridad de wpa, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4538.data"
