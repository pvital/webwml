#use wml::debian::translation-check translation="846effa858f46cf6429f61e241ec96292032972f"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.5</define-tag>
<define-tag release_date>2020-08-01</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la quinta actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Esta versión también aborda el aviso de seguridad de Debian:
<a href="https://www.debian.org/security/2020/dsa-4735">DSA-4735-1 grub2 -- actualización de seguridad</a>, 
que describe varios problemas CVE en relación con
la <a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot/">vulnerabilidad de arranque seguro UEFI con GRUB2: 'BootHole'</a>.</p>


<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction appstream-glib "Corrige fallos de compilación en compilaciones realizadas de 2020 en adelante">
<correction asunder "Utiliza gnudb en lugar de freedb por omisión">
<correction b43-fwcutter "Se asegura de que la eliminación va bien con configuraciones regionales distintas de English; no falla la eliminación si algunos ficheros no existen ya; corrige dependencias con pciutils y con ca-certificates, que faltaban">
<correction balsa "Proporciona identidad del servidor al validar certificados, permitiendo la validación satisfactoria cuando se utiliza el parche de glib-networking para CVE-2020-13645">
<correction base-files "Actualizado para esta versión">
<correction batik "Corrige falsificación de solicitudes en el lado servidor («server-side request forgery») mediante atributos xlink:href [CVE-2019-17566]">
<correction borgbackup "Corrige fallo de corrupción de índice que conduce a pérdida de datos">
<correction bundler "Actualiza la versión mínima requerida de ruby-molinillo">
<correction c-icap-modules "Añade soporte de ClamAV 0.102">
<correction cacti "Corrige problema por el que se rechazaban marcas de tiempo («timestamps») UNIX posteriores al 13 de septiembre de 2020 como inicio/fin de gráficos; corrige ejecución de código remoto [CVE-2020-7237], ejecución de scripts entre sitios («cross-site scripting») [CVE-2020-7106], problema de CSRF [CVE-2020-13231]; la inhabilitación de una cuenta de usuario no invalida los permisos inmediatamente [CVE-2020-13230]">
<correction calamares-settings-debian "Habilita el módulo displaymanager, corrigiendo opciones de autologin; usa xdg-user-dir para especificar el nombre de directorio del escritorio">
<correction clamav "Nueva versión del proyecto original; correcciones de seguridad [CVE-2020-3327 CVE-2020-3341 CVE-2020-3350 CVE-2020-3327 CVE-2020-3481]">
<correction cloud-init "Nueva versión del proyecto original">
<correction commons-configuration2 "Evita la creación de objetos durante la carga de ficheros YAML [CVE-2020-1953]">
<correction confget "Corrige la gestión que hace el módulo Python de valores que contienen <q>=</q>">
<correction dbus "Nueva versión «estable» del proyecto original; evita un problema de denegación de servicio [CVE-2020-12049]; evita «uso tras liberar» si dos nombres de usuario tienen el mismo uid">
<correction debian-edu-config "Corrige pérdida de dirección IPv4 asignada dinámicamente">
<correction debian-installer "Actualiza la ABI de Linux a la 4.19.0-10">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction debian-ports-archive-keyring "Retrasa un año la fecha de expiración de la clave 2020 (84C573CD4E1AFD6C); añade la clave Debian Ports Archive Automatic Signing Key (2021); mueve la clave 2018 (ID: 06AED62430CB581C) al anillo de claves eliminadas">
<correction debian-security-support "Actualiza el estado de soporte de varios paquetes">
<correction dpdk "Nueva versión del proyecto original">
<correction exiv2 "Ajusta parche de seguridad demasiado restrictivo [CVE-2018-10958 y CVE-2018-10999]; corrige problema de denegación de servicio [CVE-2018-16336]">
<correction fdroidserver "Corrige la validación de la dirección de Litecoin">
<correction file-roller "Corrección de seguridad [CVE-2020-11736]">
<correction freerdp2 "Corrige inicios de sesión con tarjeta inteligente; correcciones de seguridad [CVE-2020-11521 CVE-2020-11522 CVE-2020-11523 CVE-2020-11524 CVE-2020-11525 CVE-2020-11526]">
<correction fwupd "Nueva versión del proyecto original; corrige posible problema de verificación de firmas [CVE-2020-10759]; usa nuevas claves para firmar de Debian">
<correction fwupd-amd64-signed "Nueva versión del proyecto original; corrige posible problema de verificación de firmas [CVE-2020-10759]; usa nuevas claves para firmar de Debian">
<correction fwupd-arm64-signed "Nueva versión del proyecto original; corrige posible problema de verificación de firmas [CVE-2020-10759]; usa nuevas claves para firmar de Debian">
<correction fwupd-armhf-signed "Nueva versión del proyecto original; corrige posible problema de verificación de firmas [CVE-2020-10759]; usa nuevas claves para firmar de Debian">
<correction fwupd-i386-signed "Nueva versión del proyecto original; corrige posible problema de verificación de firmas [CVE-2020-10759]; usa nuevas claves para firmar de Debian">
<correction fwupdate "Usa nuevas claves para firmar de Debian">
<correction fwupdate-amd64-signed "Usa nuevas claves para firmar de Debian">
<correction fwupdate-arm64-signed "Usa nuevas claves para firmar de Debian">
<correction fwupdate-armhf-signed "Usa nuevas claves para firmar de Debian">
<correction fwupdate-i386-signed "Usa nuevas claves para firmar de Debian">
<correction gist "Evita API de autorización discontinuada («deprecated»)">
<correction glib-networking "Devuelve error de identidad errónea si identity no tiene valor [CVE-2020-13645]; rompe balsa anterior a la 2.5.6-2+deb10u1 porque la corrección para CVE-2020-13645 rompe la verificación de certificados en balsa">
<correction gnutls28 "Corrige errores de reanudación TL1.2; corrige filtración de contenido de la memoria; gestiona tickets de sesión de longitud cero, corrigiendo errores de conexión a algunos proveedores de alojamiento («hosting providers») grandes en sesiones TLS1.2; corrige error de verificación con cadenas alternativas">
<correction intel-microcode "Devuelve algunos microcódigos a versiones previas, como solución provisional a cuelgues durante el arranque en Skylake-U/Y y Skylake Xeon E3">
<correction jackson-databind "Corrige varios problemas de seguridad que afectan a BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 and CVE-2019-17267]">
<correction jameica "Añade mckoisqldb al classpath, permitiendo el uso de la extensión («plugin») SynTAX">
<correction jigdo "Corrige el soporte de HTTPS en jigdo-lite y en jigdo-mirror">
<correction ksh "Corrige problema de restricción de variables de entorno [CVE-2019-14868]">
<correction lemonldap-ng "Corrige regresión en la configuración de nginx introducida por la corrección para CVE-2019-19791">
<correction libapache-mod-jk "Renombra fichero de configuración de Apache de manera que pueda ser habilitado e inhabilitado automáticamente">
<correction libclamunrar "Nueva versión «estable» del proyecto original; añade un metapaquete sin número de versión">
<correction libembperl-perl "Trata páginas de error de Apache &gt;= 2.4.40">
<correction libexif "Correcciones de seguridad [CVE-2020-12767 CVE-2020-0093 CVE-2020-13112 CVE-2020-13113 CVE-2020-13114]; corrige desbordamiento de memoria [CVE-2020-0182] y desbordamiento de entero [CVE-2020-0198]">
<correction libinput "Quirks: añade atributo de integración de «trackpoint»">
<correction libntlm "Corrige desbordamiento de memoria [CVE-2019-17455]">
<correction libpam-radius-auth "Corrige desbordamiento de memoria en campo contraseña [CVE-2015-9542]">
<correction libunwind "Corrige violación de acceso en mips; habilita manualmente soporte de excepciones C++ solo en i386 y amd64">
<correction libyang "Corrige caída por corrupción de caché, CVE-2019-19333, CVE-2019-19334">
<correction linux "Nueva versión «estable» del proyecto original">
<correction linux-latest "Actualizado para la ABI del núcleo 4.19.0-10">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original">
<correction lirc "Corrige gestión de ficheros de configuración">
<correction mailutils "maidag: reduce privilegios de setuid para todas las operaciones de entrega excepto mda [CVE-2019-18862]">
<correction mariadb-10.3 "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2020-2752 CVE-2020-2760 CVE-2020-2812 CVE-2020-2814 CVE-2020-13249]; corrige regresión en la detección de ZSTD en RocksDB">
<correction mod-gnutls "Corrige una posible violación de acceso en un «handshake» TLS fallido; corrige fallos de pruebas">
<correction multipath-tools "kpartx: usa la ruta de partx correcta en regla udev">
<correction mutt "No comprueba cifrado de IMAP PREAUTH si está en uso $tunnel">
<correction mydumper "Enlazado con libm">
<correction nfs-utils "statd: toma user-id de /var/lib/nfs/sm [CVE-2019-3689]; no hace que el propietario de /var/lib/nfs sea statd">
<correction nginx "Corrige vulnerabilidad de «contrabando» de petición («request smuggling») de páginas de error [CVE-2019-20372]">
<correction nmap "Actualiza tamaño de clave por omisión a 2048 bits">
<correction node-dot-prop "Corrige regresión introducida por la corrección para CVE-2020-8116">
<correction node-handlebars "Prohíbe llamar directamente a <q>helperMissing</q> y a <q>blockHelperMissing</q> [CVE-2019-19919]">
<correction node-minimist "Corrige contaminación de prototipo [CVE-2020-7598]">
<correction nvidia-graphics-drivers "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2020-5963 CVE-2020-5967]">
<correction nvidia-graphics-drivers-legacy-390xx "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2020-5963 CVE-2020-5967]">
<correction openstack-debian-images "Instala resolvconf si se instala cloud-init">
<correction pagekite "Evita problemas con la expiración de los certificados SSL incluidos utilizando los del paquete ca-certificates">
<correction pdfchain "Corrige caída en el arranque">
<correction perl "Corrige varios problemas de seguridad relacionados con expresiones regulares [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Corrige vulnerabilidad de ejecución de scripts entre sitios («cross-site scripting») [CVE-2020-8035]">
<correction php-horde-gollem "Corrige vulnerabilidad de ejecución de scripts entre sitios («cross-site scripting») en la salida de breadcrumb [CVE-2020-8034]">
<correction pillow "Corrige varios problemas de lectura fuera de límites [CVE-2020-11538 CVE-2020-10378 CVE-2020-10177]">
<correction policyd-rate-limit "Corrige problemas en la contabilidad debidos a la reutilización de sockets">
<correction postfix "Nueva versión «estable» del proyecto original; corrige violación de acceso en el rol cliente de tlsproxy cuando el rol servidor está inhabilitado; corrige <q>el valor por omisión de maillog_file_rotate_suffix utilizaba el minuto en lugar del mes</q>; corrige varios problemas relacionados con TLS; correcciones de README.Debian">
<correction python-markdown2 "Corrige problema de ejecución de scripts entre sitios («cross-site scripting») [CVE-2020-11888]">
<correction python3.7 "Evita bucle infinito al leer, utilizando el módulo tarfile, ficheros TAR preparados de una manera determinada [CVE-2019-20907]; soluciona colisiones de hash en IPv4Interface y en IPv6Interface [CVE-2020-14422]; corrige problema de denegación de servicio en urllib.request.AbstractBasicAuthHandler [CVE-2020-8492]">
<correction qdirstat "Corrige grabación de categorías MIME configuradas por el usuario">
<correction raspi3-firmware "Corrige error tipográfico que podría dar lugar a sistemas que no arrancan">
<correction resource-agents "IPsrcaddr: hace que <q>proto</q> sea opcional para corregir una regresión cuando se usa sin NetworkManager">
<correction ruby-json "Corrige vulnerabilidad de creación insegura de objetos [CVE-2020-10663]">
<correction shim "Usa nuevas claves para firmar de Debian">
<correction shim-helpers-amd64-signed "Usa nuevas claves para firmar de Debian">
<correction shim-helpers-arm64-signed "Usa nuevas claves para firmar de Debian">
<correction shim-helpers-i386-signed "Usa nuevas claves para firmar de Debian">
<correction speedtest-cli "Pasa las cabeceras correctas para corregir medida de la velocidad de subida">
<correction ssvnc "Corrige escritura fuera de límites [CVE-2018-20020], bucle infinito [CVE-2018-20021], inicialización errónea [CVE-2018-20022], potencial denegación de servicio [CVE-2018-20024]">
<correction storebackup "Corrige posible vulnerabilidad de elevación de privilegios [CVE-2020-7040]">
<correction suricata "Corrige reducción de privilegios en modo de ejecución nflog">
<correction tigervnc "No usa libunwind en armel, armhf o arm64">
<correction transmission "Corrige posible problema de denegación de servicio [CVE-2018-10756]">
<correction wav2cdr "Usa tipos de entero de tamaño fijo de C99 para corregir aserción en tiempo de ejecución en arquitecturas de 64 bits distintas de amd64 y alpha">
<correction zipios++ "Corrección de seguridad [CVE-2019-13453]">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2020 4626 php7.3>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4679 keystone>
<dsa 2020 4680 tomcat9>
<dsa 2020 4681 webkit2gtk>
<dsa 2020 4682 squid>
<dsa 2020 4683 thunderbird>
<dsa 2020 4684 libreswan>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4690 dovecot>
<dsa 2020 4691 pdns-recursor>
<dsa 2020 4692 netqmail>
<dsa 2020 4694 unbound>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4696 nodejs>
<dsa 2020 4697 gnutls28>
<dsa 2020 4699 linux-signed-amd64>
<dsa 2020 4699 linux-signed-arm64>
<dsa 2020 4699 linux-signed-i386>
<dsa 2020 4699 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4707 mutt>
<dsa 2020 4708 neomutt>
<dsa 2020 4709 wordpress>
<dsa 2020 4710 trafficserver>
<dsa 2020 4711 coturn>
<dsa 2020 4712 imagemagick>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4714 chromium>
<dsa 2020 4716 docker.io>
<dsa 2020 4718 thunderbird>
<dsa 2020 4719 php7.3>
<dsa 2020 4720 roundcube>
<dsa 2020 4721 ruby2.5>
<dsa 2020 4722 ffmpeg>
<dsa 2020 4723 xen>
<dsa 2020 4724 webkit2gtk>
<dsa 2020 4725 evolution-data-server>
<dsa 2020 4726 nss>
<dsa 2020 4727 tomcat9>
<dsa 2020 4728 qemu>
<dsa 2020 4729 libopenmpt>
<dsa 2020 4730 ruby-sanitize>
<dsa 2020 4731 redis>
<dsa 2020 4732 squid>
<dsa 2020 4733 qemu>
<dsa 2020 4735 grub-efi-amd64-signed>
<dsa 2020 4735 grub-efi-arm64-signed>
<dsa 2020 4735 grub-efi-ia32-signed>
<dsa 2020 4735 grub2>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction golang-github-unknwon-cae "Problemas de seguridad; sin desarrollo activo">
<correction janus "No se puede soportar en «estable»">
<correction mathematica-fonts "Depende de una ubicación de descargas no disponible">
<correction matrix-synapse "Problemas de seguridad; no se puede soportar">
<correction selenium-firefoxdriver "Incompatible con versiones más recientes de Firefox ESR">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>
