#use wml::debian::template title="Η Γωνιά των Προγραμματιστ(ρι)ών" 
#BARETITLE="true"
#use wml::debian::translation-check translation="2402c992a4bfcd9cacd2a5f22a50a77c20d716de" maintainer="galaxico"

<p>Οι πληροφορίες στη σελίδα αυτή, αν και δημόσιες, έχουν κυρίως ενδιααφέρον 
για Προγραμματιστές/προγραμματίστριες.</p>

<ul class="toc">
<li><a href="#basic">Βασικά</a></li>
<li><a href="#packaging">Πακετάρισμα</a></li>
<li><a href="#workinprogress">Δουλειά σε εξέλιξη</a></li>
<li><a href="#projects">Έργα (Project)</a></li>
<li><a href="#miscellaneous">Διάφορα</a></li>
</ul>

<div id="main">
  <div class="cardleft" id="basic">
  <h2>Βασικά</h2>
      <div>
      <dl>
        <dt><a href="$(HOME)/intro/organization">Η Οργάνωση του Debian</a></dt>

        <dd>
        Το Debian έχει πολλά σημεία πρόσβασης και εμπλέκει πολύ κόσμο. Η σελίδα 
αυτή εξηγεί με ποια άτομα πρέπει να επικοινωνήσετε για συγκεκριμένες πτυχές 
του Debian και σας λέει ποια ενδέχεται να απαντήσουν.
        </dd>

        <dt>Τα άτομα</dt>
        <dd>
        Το Debian παράγεται με συλλογική συνεργασία από άτομα που βρίσκονται σ' 
ολόκληρο τον κόσμο. Στη <em>δουλειά σχετικά με το πακετάρισμα</em> συνεισφέρουν 
τόσο οι <a
        href="https://wiki.debian.org/DebianDeveloper">Προγραμματιστ(ρι)ες
        (DD)</a> (που είναι πλήρη μέλη του Σχεδίου Debian) όσο και οι <a
        href="https://wiki.debian.org/DebianMaintainer">Συντηρητές/τριες 
του Debian (DM)</a>. Εδώ μπορείτε να βρείτε και τη <a
        href="https://nm.debian.org/public/people/dd_all">λίστα των 
Προγραμματιστών/τριών του Debian</a> και τη <a
        href="https://nm.debian.org/public/people/dm_all">λίστα των 
Συντηρητών/τριών του Debian
        </a>, μαζί με τα πακέτα που συντηρούν.

        <p>
        Μπορείτε επίσης να δείτε τον <a 
href="developers.loc">παγκόσμιο χάρτη των προγραμματιστών/στριών του Debian
        </a> καθώς και <a href="https://gallery.debconf.org/">συλλογές 
φωτογραφιών</a> από διάφορες εκδηλώσεις σχετικές με το Debian.
        </p>
        </dd>

        <dt><a href="join/">Μπείτε στο Debian</a></dt>

        <dd>
        Το Σχέδιο Debian συγκροτείται από εθελοντές και γενικά ψάχνουμε για 
        καινούριους/ες προγραμματιστές/προγραμματίστριες που να έχουν μια 
        κάποια τεχνική γνώση, ενδιαφέρον για το ελεύθερο λογισμικό και λίγο 
        ελεύθερο χρόνο. Μπορείτε επίσης να βοηθήσετε το Debian, δείτε απλά τον 
σύνδεσμο στη σελίδα παραπάνω.
        </dd>

        <dt><a href="https://db.debian.org/">Βάση Δεδομένων των 
προγραμματίστ(ρι)ών </a></dt>
        <dd>
        Αυτή η βάση δεδομένων περιέχει κάποια βασικά δεδομένα προσβάσιμα σε 
όλους, και πιο προσωπικά δεδομένα που είναι διαθέσιμα μόνο στους άλλους/άλλες 
προγραμματιστές/προγραμματίστριες για να τα δουν. Χρησιμοποιήστε την 
        <a href="https://db.debian.org/">SSL έκδοση</a> για πρόσβαση, αν θέλετε 
να μπείτε στη βάση.

        <p>Χρησιμοποιώντας τη βάση, μπορείτε να δείτε τη λίστα των 
        <a href="https://db.debian.org/machines.cgi">μηχανημάτων του 
Σχεδίου</a>,
        <a href="extract_key">να πάρετε το GPG κλειδί 
οποιουδήποτε/οποιασδήποτε προγραμματιστή/προγραμματίστριας</a>,
        <a href="https://db.debian.org/password.html">να αλλάξετε τον 
κωδικό πρόσβασής σας</a>
        ή να <a href="https://db.debian.org/forward.html">μάθετε πώς να 
ρυθμίσετε την προώθηση αλληλογραφίας</a> για τον λογαριασμό σας στο Debian.</p>

        <p>Αν πρόκειται να χρησιμοποιήσετε ένα από τα μηχανήματα του Debian 
βεβαιωθείτε ότι έχετε διαβάσει τις <a href="dmup">Πολιτικές χρήσης των 
μηχανημάτων του Debian</a>.</p>
        </dd>

        <dt><a href="constitution">Το Καταστατικό</a></dt>
        <dd>
        Το πλέον σημαντικό κείμενο για τον οργανισμό του Debian, περιγράφει την 
οργανωτική δομή για την τυπική διαδικασία λήψης αποφάσεων στο Σχέδιο.
        </dd>

        <dt><a href="$(HOME)/vote/">Πληροφορίες για τις Ψηφοφορίες</a></dt>
        <dd>
        Οτιδήποτε θα θέλατε να ξέρετε για το πώς εκλέγουμε τον/την Επικεφαλής 
μας, επιλέγουμε τα λογότυπά μας και, γενικά, για το πώς ψηφίζουμε.
        </dd>
     </dl>

# this stuff is really not devel-only
     <dl>
        <dt><a href="$(HOME)/releases/">Εκδόσεις</a></dt>

        <dd>
        Αυτή είναι μια λίστα με τις παλιές και τρέχουσες εκδόσεις, για μερικές 
από τις οποίες υπάρχουν λεπτομερείς πληροφορίες σε ξεχωριστές σελίδες.

        <p>Μπορείτε επίσης να πάτε απευθείας στις ιστοσελίδες της τρέχουσας 
        <a href="$(HOME)/releases/stable/">σταθερής έκδοσης</a> και της
        <a href="$(HOME)/releases/testing/">δοκιμαστικής διανομής</a>.</p>
        </dd>

        <dt><a href="$(HOME)/ports/">Διαφορετικές αρχιτεκτονικές</a></dt>

        <dd>
        Το Debian τρέχει σε πολλά είδη υπολογιστών (οι συμβατοί 
με Intel ήταν απλά το <em>πρώτο</em> είδος), και οι συντηρητές των διαφόρων 
        &lsquo;υλοποιήσεων&rsquo; έχουν κάποιες χρήσιμες ιστοσελίδες. Ρίξτε μια 
ματιά, ίσως θελήσετε να αποκτήσετε ένα άλλο μηχάνημα με περίεργο όνομα για τον 
εαυτό σας.
	</dd>
      </dl>
      </div>

  </div>

  <div class="cardright" id="packaging">
     <h2>Πακετάρισμα</h2>
     <div>

      <dl>
        <dt><a href="$(DOC)/debian-policy/">Εγχειρίδιο πολιτικής 
του Debian</a></dt>
        <dd>
        Αυτό το εγχειρίδιο περιγράφει την απαιτήσεις πολιτικής για τη 
διανομή του Debian. Αυτό περιλαμβάνει την δομή και τα περιεχόμενα της 
αρχειοθήκης του Debian, αρκετά ζητήματα σχεδιασμού του λειτουργικού 
συστήματος καθώς και τεχνικές απαιτήσεις που το κάθε πακέτο θα πρέπει να 
ικανοποιεί για να συμπεριληφθεί στη διανομή.

        <p>Με λίγα λόγια, <strong>πρέπει</strong> να το διαβάσετε.</p>
        </dd>
      </dl>

      <p>Υπάρχουν αρκετά κείμενα σχετικά με την Πολιτική που ίσως θα σας 
ενδιέφεραν, όπως τα::</p>
      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />Το FHS είναι μια λίστα καταλόγων (ή αρχείων) όπου πρέπει να 
τοποθετηθούν διάφορα πράγματα, και η Πολιτική έκδοση 3.x απαιτεί τη συμβατότητα 
με αυτό.</li>
        <li>Λίστα <a 
href="$(DOC)/packaging-manuals/build-essential">build-essential πακέτων</a>
        <br />Τα πακέτα στη σουίτα build-essential είναι πακέτα που θα πρέπει 
να διαθέτετε πριν προσπαθήσετε να φτιάξετε οποιοδήποτε πακέτο ή σύνολο πακέτων 
που δεν χρειάζεται να το προσθέσετε στη γραμμή             
<code>Build-Depends</code> του πακέτου σας.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">Μενού συστήματος</a>
        <br />Προγράμματα που έχουν μια διεπαφή στην οποία δεν χρειάζεται να περαστούν οποιαδήποτε ειδικά ορίσματα γραμμής εντολών ώστε να λειτουργούν κανονικά, θα πρέπει να έχουν καταχωρημένη μια είσοδο στο μενού.
            Ελέγξτε επίσης και την <a href="$(DOC)/packaging-manuals/menu.html/">τεκμηρίωση του μενού του συστήματος</a>.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Πολιτική του Emacs</a>
        <br />Τα πακέτα που σχετίζονται με τον Emacs υποτίθεται ότι θα πρέπει να συμμορφώνονται με το κείμενο της δικής τους υπο-πολιτικής.</li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Πολιτική Java</a>
        <br />Το προτεινόμενο ισοδύμανο του παραπάνω, για τα σχετιζόμενα με την Java πακέτα.</li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Πολιτική Perl</a>
        <br />Μια υπο-πολιτική που καλύπτει οτιδήποτε έχει να κάνει με το 
πακετάρισμα της Perl.</li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Πολιτική Python</a>
        <br />Μια προταθείσα υπο-πολιτική που καλύπτει οτιδήποτε σχετικό με το 
πακετάρισμα της Python.</li>
#	<li><a href="https://pkg-mono.alioth.debian.org/cli-policy/">Πολιτική του Debian για την CLI</a>
#	<br />Basic policies regarding packaging Mono, other CLRs and
#        CLI based applications and libraries</li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Προδιαγραφές του Debconf</a>
        <br />Οι προδιαγραφές για το υποσύστημα διαχείρισης ρυθμίσεων
            "debconf".</li>
#        <li><a href="https://dict-common.alioth.debian.org/">Spelling dictionaries and tools policy</a>
#        <br />Sub-policy for <kbd>ispell</kbd> / <kbd>myspell</kbd> dictionaries and word lists.</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft/html/">Webapps Policy Manual</a> (draft)
#	<br />Sub-policy for web-based applications.</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft-php/html/">PHP Policy</a> (draft)
#	<br />Packaging standards of PHP.</li>
	<li><a 
href="https://www.debian.org/doc/manuals/dbapp-policy/">Πολιτική 
Εφαρμογών Βάσεων Δεδομένων</a> (draft)
	<br />Ένα σύνολο από κατευθύνσεις και βέλτιστες πρακτικές για πακέτα 
εφαρμογών βάσεων δεδομένων.</li>
	<li><a 
href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">
Πολιτική Tcl / Tk </a> (draft)
	<br />Υπο-πολιτική που καλύπτει οτιδήποτε αφορά το πακετάρισμα της 
Tcl/Tk.</li>
	<li><a
	
href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Πολιτική 
του Debian για την Ada</a>
	<br />Υπο-πολιτική που καλύπτει οτιδήποτε αφορά το πακετάρισμα της Ada.</li>
      </ul>

      <p>Ρίξτε επίσης μια ματιά <a 
href="https://bugs.debian.org/debian-policy"> στις προτεινόμενες αλλαγές 
στην Πολιτική</a>, too.</p>

      <p>Σημειώστε ότι το παλιό Εγχειρίδιο Πολιτικής έχει ως επί το πλείστον 
ενσωματωθεί στις πρόσφατες εκδόσεις του Εγχειριδίου Πολιτικής.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">
        Αναφορά των Προγραμματιστ(ρι)ών</a></dt>

        <dd>
        Ο σκοπός αυτού του κειμένου είναι να προσφέρει μια επισκόπηση των 
        συνιστώμενων διαδικασιών και των διαθέσιμων πόρων για τους/τις 
προγραμματίστριες του. Ένα ακόμα κείμενο που <strong>πρέπει</strong> να 
διαβαστεί.
        </dd>

        <dt><a href="$(DOC)/manuals/maint-guide/">Οδηγός Νέων 
Συντηρητ(ρι)ών</a></dt>

        <dd>
        Αυτό το κείμενο περιγράφει την κατασκευή ενός πακέτου Debian με 
απλά λόγια, και καλύπτεται επαρκώς με αρκετά παραδείγματα. Αν είστε επίδοξος/η 
προγραμματιστής/στρια (packager), θα θέλετε οπωσδήποτε να το διαβάσετε.
        </dd>
      </dl>
      </div>

  </div>

  <div class="card" id="workinprogress">
      <h2>Δουλειά&nbsp;σε&nbsp;εξέλιξη</h2>
      <div>

	<dl>
        <dt><a href="testing">Η δοκιμαστική διανομή</a></dt>
        <dd>
        Η &lsquo;δοκιμαστική&rsquo; διανομή είναι το σημείο στο οποίο πρέπει να 
ανεβάσετε τα πακέτα σας ώστε να ληφθούν υπόψιν την επόμενη φορά 
που το Debian θα κυκλοφορήσει μια καινούρια έκδοση.
        </dd>

        <dt><a 
href="https://bugs.debian.org/release-critical/">Σφάλματα κρίσιμα για 
την κυκλοφορία της έκδοσης</a></dt>

        <dd>
        Αυτή είναι μια σελίδα με σφάλματα που πιθανόν να προκαλέσουν την 
αφαίρεση ενός πακέτου από τη "δοκιμαστική" διανομή, ή σε μερικές 
περιπτώσεις να καθυστηερήσουν ακόμα και την έκδοση της διανομής. Αναφορές 
σφαλμάτων με σοβαρότητα υψηλότερη ή ίση με &lsquo;serious&rsquo; μπορούν να 
συμπεριληφθούν στη λίστα -- βεβαιωθείτε να διορθώσετε οποιαδήποτε τέτοια 
σφάλματα σχετικά με τα πακέτα σας όσο πιο γρήγορα μπορείτε.
        </dd>

        <dt><a href="$(HOME)/Bugs/">Το Σύστημα Παρακολούθησης Σφαλμάτων</a></dt>
        <dd>
        Το Σύστημα Παρακολούθησης Σφαλμάτων (Debian Bug Tracking System, BTS) το ίδιο, για αναφορές, συζητήσεις και διορθώσεις σφαλμάτων. Αναφορές για σχεδόν οποιοδήποτε τμήμα του Debian είναι ευπρόσδεκτες εδώ. Το BTS είναι χρήσισμο τόσο για χρήστες όσο και προγραμματιστές/προγραμματίστριες.
        </dd>

        <dt>Επισκόπηση πακέτων, από τη σκοπιά των προγραμματι(στρι)ών</dt>
        <dd>
	Οι ιστοσελίδες <a href="https://qa.debian.org/developer.php">package 
information</a>
        και <a href="https://tracker.debian.org/">package tracker</a> παρέχουν
        συλλογές πολύτιμων πληροφοριών για τους/τις συντηρητές/τριες πακέτων.
        </dd>

        <dt><a
href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">Το 
Σύστημα Παρακολούθησης πακέτων του Debian</a></dt>
        <dd>
        Για προγραμματιστές/προγραμματίστριες που θέλουν να είναι 
ενημερωμένοι/ες για άλλα πακέτα, ο ινχηλάτης πακέτων τους επιτρέπει να 
εγγράφονται (μέσω email) σε μια υπηρεσία που θα τους στέλνει αντίγραφα των 
μηνυμάτων και ειδοποιήσεων για μεταφορτώσεις και εγκαταστάσεις που αφορούν τα 
πακέτα για τα οποία έχουν εγγραφεί.
        </dd>

        <dt><a href="wnpp/">Πακέτα που χρειάζονται βοήθεια</a></dt>
        <dd>
        Work-Needing and Prospective Packages, σε συντομία WNPP, είναι μια 
        λίστα
        πακέτων Debian που χρειάζονται νέους συντηρητές καθώς και των πακέτων 
που δεν έχουν ακόμα συμπεριληφθεί στο Debian. Ελέγξτε την αν θέλετε να 
δημιουργήσετε, να υιοθετήσετε ή να εγκαταλείψετε κάποια πακέτα.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">\
            Σύστημα Εισερχομένων</a></dt>
        <dd>
        Καινούρια πακέτα ανεβαίνουν στο σύστημα "Εισερχομένων" των εσωτερικών εξυπηρετητών των αρχειοθηκών. Τα πακέτα που έχουν γίνονται δεκτά είναι σχεδόν αμέσως 
        <a href="https://incoming.debian.org/">διαθέσιμα μέσω HTTP</a>,
        και διαδίδονται στους  <a href="$(HOME)/mirror/">καθρέφτες</a> τέσσερις φορές την ημέρα.
        <br />
        <strong>Σημείωση</strong>: Εξαιτάις της φύσης των Εισερχομένων, δεν συνιστούμε τον κατοπτρισμό τους.
        </dd>

        <dt><a href="https://lintian.debian.org/">Αναφορές Lintian</a></dt>

        <dd>
        Το <a href="https://packages.debian.org/unstable/devel/lintian">
        Lintian</a> είναι ένα πρόγραμμα που ελέγχει αν ένα πακέτο συμμορφώνεται στην Πολιτική του Debian. Θα πρέπει να το χρησιμοποιείτε πριν από κάθε ανέβασμα· στην σελίδα που προαναφέραμε υπάρχουν αναφορές για κάθε πακέτο στη διανομή.
        </dd>

        <dt><a href="https://wiki.debian.org/HelpDebian">Βοηθήστε το 
Debian</a></dt>
        <dd>
	Tο wiki του Debian συλλέγει συμβουλές και οδηγίες για προγραμματιστές/τριες και άλλα άτομα που συνεισφέρουν στο Debian.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">\
            Η πειραματική διανομή</a></dt>
        <dd>
        Η <em>πειραματική</em> διανομή χρησιμοποιείται ως μια προσωρινή περιοχή δοκιμής για εξαιρετικά πειραματικό λογισμικό. Χρησιμοποιείστε τα
        <a href="https://packages.debian.org/experimental/">πακέτα από την
        <em>πειραματική</em></a> διανομή μόνο αν ξέρετε ήδη πώς να χρησιμοποιείτε την 
        <em>ασταθή</em>.
        </dd>
      </dl>
      </div>

  </div>
  <div class="card" id="projects">
     <h2>Έργα</h2>
     <div>

      <p>Το Debian είναι μια μεγάλη ομάδα, και ως τέτοια, συγκροτείται από αρκετές εσωτερικές ομάδες και έργα. Αυτά είναι τα έργα που έχουν μια δική τους ιστοσελίδα, με χρονολογική σειρά:</p>
      <ul>
          <li><a href="website/">Ιστοσελίδες του Debian</a></li>
          <li><a href="https://ftp-master.debian.org/">Αρχειοθήκη 
του Debian</a></li>
          <li><a href="$(DOC)/ddp">Το Σχέδιο Τεκμηρίωσης του 
Debian (DDP)</a></li>
          <li><a href="https://qa.debian.org/">Η ομάδα Διασφάλισης Ποιότητας</a>
            </li>
          <li><a href="$(HOME)/CD/">Εικόνες CD του Debian</a></li>
          <li>Η σελίδα <a href="https://wiki.debian.org/Keysigning">συντονισμού της υπογραφής κλειδιών</a></li>
          <li><a href="https://wiki.debian.org/DebianIPv6">Σχεδιο IPv6 του 
Debian </a></li>
          <li><a href="buildd/">Το δίκτυο auto-builder</a> και τα <a href="https://buildd.debian.org/">build log</a></li>
          <li><a href="$(HOME)/international/l10n/ddtp">Debian Description Translation Project
              (DDTP)</a></li>
	  <li><a href="debian-installer/">Ο Εγκαταστάτης του Debian</a></li>
	  <li><a href="debian-live/">Debian Live</a></li>
	  <li><a href="$(HOME)/women/">Γυναίκες στο Debian</a></li>

	</ul>

	<p>Ένας αριθμός από αυτά τα έργα σκοπεύουν να δημιουργήσουν <a 
href="https://wiki.debian.org/DebianPureBlends">Καθαρά Μείγματα Debian
	</a> για μια συγκεκριμένη ομάδα χρηστών ενώ συνεχίζουν να δουλεύουν εξ 
ολοκλήρου μέσα σε ένα σύστημα Debian. Αυτά περιλαμβάνουν τα:</p>

	<ul>
	  <li><a href="debian-jr/">Σχέδιο Debian Jr</a></li>
          <li><a href="debian-med/">Σχέδιο Debian Med</a></li>
          <li><a href="https://wiki.debian.org/DebianEdu">Σχέδιο Debian 
Edu/Skolelinux</a></li>
	  <li><a href="debian-accessibility/">Σχέδιο Προσβασιμότητας του 
Debian</a></li>
	  <li><a href="https://wiki.debian.org/DebianGis">Σχέδιο Debian GIS</a></li>
	  <li><a href="https://wiki.debian.org/DebianScience">Σχέδιο Debian 
Science</a></li>
	  <li><a 
href="https://salsa.debian.org/debichem-team/team/debichem/">Σχέδιο 
DebiChem</a></li>
	</ul>
	</div>

  </div>

  <div class="card" id="miscellaneous">
      <h2>Διάφορα</h2>
      <div>

      <p>Assorted links:</p>
      <ul>
        <li><a 
href="https://debconf-video-team.pages.debian.net/videoplayer/">Βιντεοσκοπήσεις 
</a> των ομιλιών στα συνέδριά μας.</li>
        <li><a href="passwordlessssh">Ρυθμίστε το ssh ώστε να μην σας ζητά 
κωδικό πρόσβασης</a>.</li>
        <li> <a href="$(HOME)/MailingLists/HOWTO_start_list">Πώς να αιτηθείτε 
μια καινούρια λίστα αλληλογραφίας</a>.</li>
        <li> <a href="https://dsa.debian.org/iana/">Ιεραρχία OID του Debian
            </a>.</li>
        <li>Πληροφορίες για τους <a href="$(HOME)/mirror/">καθρέφτες του 
Debian</a>.</li>
        <li> <a 
href="https://qa.debian.org/data/bts/graphs/all.png">Το γράφημα όλων των 
σφαλμάτων</a>.</li>
	<li><a href="https://ftp-master.debian.org/new.html">Καινούρια πακέτα σε 
αναμονή να συμπεριληφθούν στο Debian</a> (NEW queue).</li>
        <li><a 
href="https://packages.debian.org/unstable/main/newpkg">Καινούρια πακέτα 
Debian από τις τελευταίες 7 μέρες</a>.</li>
        <li><a href="https://ftp-master.debian.org/removals.txt">Πακέτα που 
αφαιρέθηκαν από το Debian</a>.</li>
        </ul>

      </div>

  </div>
</div>
