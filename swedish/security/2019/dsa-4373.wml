#use wml::debian::translation-check translation="08e97d6a66338b9fb8da51eb27b4c3dde971c164" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i coTURN, en TURN- och STUN-server för
VoIP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4056">CVE-2018-4056</a>

	<p>En SQL-injiceringssårbarhet har upptäckts i coTURNs
	administratörswebbportal. Eftersom administrationswebbgränssnittet delas
	med produktionen är det inte möjligt att lätt filtrera åtkomst utifrån
	och denna säkerhetsuppdatering inaktiverar webbgränssnittet fullständigt.
	Användare bör använda det lokala kommandoradsgränssnittet istället.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>

	<p>Standardkonfigurationen aktiverar osäker forwarding av loopbacks.
	En fjärrangripare med åtkomst till TURN-gränssnittet kan använda denna
	sårbarhet för att få åtkomst till tjänster som bör vara endast lokala.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4059">CVE-2018-4059</a>

	<p>Standardkonfigurationen använder ett tomt lösenord för det lokala
	kommandoradsgränssnittet. En angripare med åtkomst till den lokala
	konsolen (antingen en lokal angripare eller en fjärrangripare som drar
	fördel av <a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>)
	kan eskalera rättigheterna till administratör av coTURN-servern.</p></li>

</ul>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 4.5.0.5-1+deb9u1.</p>

<p>Vi rekommenderar att ni uppgraderar era coturn-paket.</p>

<p>För detaljerad säkerhetsstatus om coturn vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/coturn">\
https://security-tracker.debian.org/tracker/coturn</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4373.data"
