#use wml::debian::translation-check translation="5ff7a077f0e13d9fe9cae52a517d81e9a15c05d7" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i Linuxkärnan som kan leda till
utökning av privilegier, överbelastning eller informationsläckage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14821">CVE-2019-14821</a>

    <p>Matt Delco rapporterade en kapplöpningseffekt i KVM's koalescerade
    MMIO-facilitet, vilket kunde leda till åtkomst utanför avgränsningarna
    i kärnan. En lokal angripare med åtkomst till /dev/kvm kunde använda
    detta för att orsaka en överbelastning (minneskorruption eller krasch)
    eller möjligen för utökning av privilegier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14835">CVE-2019-14835</a>
    
    <p>Peter Pi från Tencent Blade-gruppen upptäckte en saknad gränskontroll
    i vhost_net, nätverksbackenddrivrutinen för KVM-värdar, vilket leder
    till buffertspill när värden startar en live-migration av en VM.
    En angripare med kontroll över en VM kunde utnyttja detta för att
    orsaka en överbelastning (minneskorruption eller krasch) eller möjligen
    för rättighetseskalering på värden.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15117">CVE-2019-15117</a>

    <p>Hui Peng och Mathias Payer rapporterade att saknad gränskontroll i
    usb-audio-drivrutinens deskriptortolkande kod leder till en
    buffer överläsning. En angripare med möjlighet att lägga till
    USB-enheter kunde möjligen utnyttja detta för att orsaka
    överbelastning (krasch).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15118">CVE-2019-15118</a>

    <p>Hui Peng och Mathias Payer rapporterade icke begränsad rekursion
    i usb audio-drivrutinens deskriptortolkande kod, vilket leder
    till stackspill. En angripare med möjlighet att lägga till USB-enheter
    kunde utnyttja detta för att orsaka överbelastning (minneskorruption eller
    krasch) eller möjligen för utökning av rättigheter. På arkitekturen
    amd64 och på arm64 i Buster, lindras detta av en skyddssida på
    kärnstacken, så det endast möjligt att orsaka krasch.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15902">CVE-2019-15902</a>

    <p>Brad Spengler rapporterade att ett bakåtanpassningsfel
    återintroducerade en spectre-v1-sårbarhet i undersystemet ptrace i
    funktionen ptrace_get_debugreg().</p></li>

</ul>

<p>För den gamla stabila utgåvan (Stretch) har dessa problem rättats
i version 4.9.189-3+deb9u1.</p>

<p>För den stabila utgåvan (Buster) har dessa problem rättats i
version 4.19.67-2+deb10u1.</p>

<p>Vi rekommenderar att ni uppgraderar era linux-paket.</p>

<p>För detaljerad säkerhetsstatus om linux vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4531.data"
