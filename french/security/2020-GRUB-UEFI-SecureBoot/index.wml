#use wml::debian::translation-check translation="9c47c948da90cea57112872aa23f03da3a967d7b" maintainer="Jean-Pierre Giraud"
#use wml::debian::template title="Vulnérabilité de UEFI SecureBoot dans GRUB2 — « BootHole »"

<p>
Des développeurs de Debian et d'ailleurs dans la communauté Linux ont
récemment pris conscience d'un problème sévère dans le chargeur d'amorçage
GRUB2 qui permet à un acteur malfaisant de complètement contourner
l'amorçage sécurisé (<q>Secure Boot</q> — SB) avec UEFI. Les détails
complets du problème sont décrits dans l'<a
href="$(HOME)/security/2020/dsa-4735">annonce de sécurité de Debian
4735</a>. Le but de ce document est d'expliquer les conséquences de cette
vulnérabilité de sécurité et les étapes pour sa suppression.
</p>

<ul>
  <li><b><a href="#what_is_SB">Contexte : qu'est-ce que l'amorçage sécurisé avec UEFI ?</a></b></li>
  <li><b><a href="#grub_bugs">Plusieurs bogues découverts dans GRUB2</a></b></li>
  <li><b><a href="#linux_bugs">Des bogues dans Linux également</a></b></li>
  <li><b><a href="#revocations">Des révocations de clés nécessaires pour corriger la chaîne d'amorçage sécurisé</a></b></li>
  <li><b><a href="#revocation_problem">Quels sont les effets de la révocation de clés ?</a></b></li>
  <li><b><a href="#package_updates">Paquets mis à jour</a></b>
  <ul>
    <li><b><a href="#grub_updates">1. GRUB2</a></b></li>
    <li><b><a href="#linux_updates">2. Linux</a></b></li>
    <li><b><a href="#shim_updates">3. Shim</a></b></li>
    <li><b><a href="#fwupdate_updates">4. Fwupdate</a></b></li>
    <li><b><a href="#fwupd_updates">5. Fwupd</a></b></li>
  </ul></li>
  <li><b><a href="#buster_point_release">Version intermédiaire
        Debian 10.5 (<q>Buster</q>) des médias d'installation et autonomes
        mis à jour</a></b></li>
  <li><b><a href="#more_info">Plus d'informations</a></b></li>
</ul>

<h1><a name="what_is_SB">Contexte : qu'est-ce que l'amorçage sécurisé avec UEFI ?</a></h1>

<p>
UEFI Secure Boot (SB) est un mécanisme de vérification pour s'assurer que
le code chargé par le microcode UEFI d'un ordinateur est de confiance. Il
est conçu pour protéger une machine contre du code malveillant qui serait
chargé et exécuté dans le processus d'amorçage initial, avant que le
système d'exploitation ne soit chargé.
</p>

<p>
SB fonctionne à l'aide de sommes de contrôle et de signatures de
chiffrement. Chaque programme chargé par le microcode comprend une
signature et une somme de contrôle, et avant d'en permettre l'exécution,
le microcode vérifie que le programme est de confiance en validant la
somme de contrôle et la signature. Quand SB est actif sur une machine,
toute tentative pour exécuter un programme qui n'est pas de confiance
est interdite. Cela empêche que du code non prévu ou non autorisé ne soit
exécuté dans l'environnement d'UEFI.
</p>

<p>
La plupart des matériels X86 sont livrés d'usine préchargés avec les clés
de Microsoft. Cela signifie que le microcode présent sur ces machines
fera confiance aux binaires signés par Microsoft. Les machines les plus
modernes sont livrées avec SB activé — par défaut, elles n'exécutent
aucun code non signé, mais il est possible de modifier la configuration
du microcode soit pour désactiver SB, soit pour inscrire des clés
supplémentaires.
</p>

<p>
Debian, comme beaucoup d'autres systèmes d'exploitation basés sur Linux,
utilise un programme nommé shim pour étendre ce système de confiance du
microcode à d'autres programmes qui nécessitent d'être sécurisés durant
l'amorçage initial : le chargeur d'amorçage GRUB2, le noyau Linux et les
outils de mise à jour du microcode (fwupd et fwupdate).
</p>

<h1><a name="grub_bugs">Plusieurs bogues découverts dans GRUB2</a></h1>

<p>
Malheureusement, un bogue sérieux a été découvert dans le code du chargeur
d'amorçage GRUB2 qui lit et analyse sa configuration (grub.cfg). Ce bogue
brise la chaîne de confiance ; son exploitation permet de casser
l'environnement sécurisé et de charger des programmes non signés durant
l'amorçage initial. Cette vulnérabilité a été découverte par des
chercheurs d'Eclypsium et a reçu le nom de
<b><a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">BootHole</a></b>.
</p>

<p>
Plutôt que de corriger uniquement ce bogue, les développeurs ont été
incités à réaliser un audit en profondeur du code source de GRUB2. Il
aurait été irresponsable de corriger un défaut majeur sans également en
rechercher d'autres ! Une équipe d'ingénieurs a travaillé pendant
plusieurs semaines pour identifier et corriger toute une série d'autres
problèmes. Ils ont découvert quelques emplacements où des allocations de
mémoire interne pourraient subir des dépassements avec des entrées non
prévues, et plusieurs autres emplacements où un dépassement d'entier dans
des calculs mathématiques pourrait causer des problèmes, et enfin
quelques emplacements où la mémoire pourrait être utilisée après sa
libération. De correctifs pour tout cela ont été partagés et testés dans
toute la communauté.
</p>

<p>
De nouveau, veuillez consulter l'<a
href="$(HOME)/security/2020/dsa-4735">annonce de sécurité de Debian 4735</a>
pour une liste complète des problèmes découverts.
</p>


<h1><a name="linux_bugs">Des bogues dans Linux également</a></h1>

<p>
Pendant la discussion sur les défauts de GRUB2, les développeurs ont aussi
parlé de deux contournements récemment découverts et corrigés par
Jason A. Donenfeld (zx2c4)
(<a href="https://git.zx2c4.com/american-unsigned-language/tree/american-unsigned-language.sh">1</a>, <a href="https://git.zx2c4.com/american-unsigned-language/tree/american-unsigned-language-2.sh">2</a>)
dans lesquels Linux pourrait aussi contourner le <q>Secure Boot</q>. Tous
les deux permettent au superutilisateur de remplacer des tables ACPI dans
un système verrouillé alors qu'il ne devrait pas y être autorisé. Des
correctifs ont déjà été publiés pour ces problèmes.
</p>

<h1><a name="revocations">Des révocations de clés nécessaires pour corriger la chaîne d'amorçage sécurisé</a></h1>

<p>
Debian et d'autres fournisseurs de systèmes d'exploitation vont évidemment
<a href="#package_updates">publier des versions corrigées</a> de GRUB2 et
Linux. Néanmoins, cela ne peut pas corriger complètement le problème traité
ici. Des acteurs malveillants pourraient encore utiliser des versions
vulnérables plus anciennes de GRUB2 et Linux pour pouvoir contourner
l'amorçage sécurisé.
</p>

<p>
Pour mettre un terme à cela, l'étape suivante pour Microsoft sera de
bloquer ces binaires non sûrs pour les empêcher d'être exécutés
par SB. Cette démarche est réussie en chargeant la liste <b>DBX</b>, une
fonctionnalité de la conception d'UEFI Secure Boot. Il a été demandé
à toutes les distributions Linux livrées avec des copies de shim signées par
Microsoft de fournir des détails sur les binaires ou les clés concernés
pour faciliter ce processus. Le <a
href="https://uefi.org/revocationlistfile">fichier de la liste de
révocation d'UEFI</a> sera mis à jour pour inclure cette information. À un
<b>certain</b> moment dans le futur, les machines commenceront à utiliser
cette liste mise à jour et refuseront d'exécuter les binaires vulnérables
avec Secure Boot.
</p>

<p>
La chronologie <i>exacte</i> du déploiement de ce changement n'est pas
encore claire. Les fournisseurs de BIOS et UEFI vont inclure la nouvelle
liste de révocation dans les constructions de microcode pour le matériel
neuf à un certain moment. Microsoft <b>peut</b> aussi publier des mises
à jour pour des systèmes existants au moyen de Windows Update. Il est
possible que certaines distributions Linux fassent des mises à jour
à l'aide de leur propre processus de mises à jour de sécurité. Debian n'a
<b>pas encore</b> fait cela, mais nous nous penchons sur la question pour
le futur.
</p>

<h1><a name="revocation_problem">Quels sont les effets de la révocation de clés ?</a></h1>

<p>
La plupart des fournisseurs hésitent à appliquer automatiquement des mises
à jour qui révoquent les clés utilisées pour Secure Boot. Les installations
logicielles existantes avec SB activé peuvent brusquement refuser
totalement de démarrer, à moins que l'utilisateur prenne soin d'installer
également toutes les mises à jour logicielles nécessaires. Les machines
bénéficiant du <q>dual boot</q> Windows/Linux peuvent soudain cesser de
démarrer avec Linux. Les anciens médias d'installation et les médias
autonomes échoueront bien sûr aussi à démarrer, rendant potentiellement
encore plus difficile la récupération des systèmes.
</p>

<p>
Il y a deux manières évidentes de corriger une machine qui refuse de
démarrer :
</p>

<ul>
  <li>redémarrer en mode <q>rescue</q> en utilisant un
    <a href="#buster_point_release">média d'installation récent</a> et
    appliquer les mises à jour de cette manière ;</li>
  <li>ou désactiver temporairement l'amorçage de sécurité pour recouvrer un
    accès à la machine, appliquer les mises à jour puis le réactiver.</li>
</ul>

<p>
Ces deux options semblent simples, mais chacune peut prendre beaucoup de
temps aux utilisateurs qui ont de multiples machines. Aussi soyez conscient
qu'activer ou désactiver Secure Boot nécessite, de par sa conception, un
accès direct à la machine. Il n'est normalement <b>pas</b> possible de
modifier cette configuration en dehors du réglage du microcode de
l'ordinateur. Pour cette raison précise, les machines qui sont des serveurs
distants doivent faire l'objet d'un soin particulier.
</p>

<p>
Pour ces raisons, il est fortement recommandé à <b>tous</b> les
utilisateurs de Debian de prendre la précaution d'installer toutes les
<a href="#package_updates">mises à jour recommandées</a> pour leurs
machines dès que possible, afin de réduire le risque de problèmes
à l'avenir.
</p>

<h1><a name="package_updates">Paquets mis à jour</a></h1>

<p>
<b>Note :</b> Les machines qui fonctionnent avec Debian 9 (<q>Stretch</q>)
et les versions antérieures ne recevront forcément <b>pas</b> de mise
à jour dans la mesure où Debian 10 (<q>Buster</q>) est la première des
versions de Debian à inclure la prise en charge de UEFI Secure Boot.
</p>

<p>La version signée de tous les paquets cités a été mise à jour, même si
aucune autre modification n'est nécessaire. Il reste à Debian de générer de
nouveaux certificats et clés de signature pour ses propres paquets
<q>Secure Boot</q>. L'ancien certificat était étiqueté <q>Debian Secure
Boot Signer</q> (empreinte numérique
<code>f156d24f5d4e775da0e6a9111f074cfce701939d688c64dba093f97753434f2c</code>);
le nouveau certificat est <q>Debian Secure Boot Signer 2020</q>
(<code>3a91a54f9f46a720fe5bbd2390538ba557da0c2ed5286f5351fe04fff254ec31</code>). </p>

<p>
Cinq paquets source de Debian seront mis à jour du fait des modifications
de UEFI Secure Boot décrites ici :
</p>

<h2><a name="grub_updates">1. GRUB2</a></h2>

<p>
Les paquets des versions mises à jour de GRUB2 de Debian sont maintenant
disponibles au moyen de l'archive debian-security pour la version stable
Debian 10 (<q>Buster</q>). Les versions corrigées seront très bientôt dans
l'archive normale de Debian pour les versions de développement de Debian
(unstable et testing).
</p>

<h2><a name="linux_updates">2. Linux</a></h2>

<p>
Les paquets des versions mises à jour de linux de Debian sont maintenant
disponibles au moyen de buster-proposed-updates pour la version stable
Debian 10 (<q>Buster</q>) et seront inclus dans la version
intermédiaire 10.5 à venir. De nouveaux paquets sont aussi dans l'archive
Debian pour les versions de développement de Debian (unstable et testing).
Nous espérons avoir corrigé également bientôt les paquets versés dans
buster-backports.
</p>

<h2><a name="shim_updates">3. Shim</a></h2>

<p>
En raison du mode de fonctionnement de la gestion de clés de Secure Boot
dans Debian, la distribution n'aura <b>pas</b> besoin de révoquer les
paquets shim existants signés par Microsoft. Néanmoins, les versions
signées des paquets shim-helper avaient besoin d'être reconstruites pour
utiliser la nouvelle clé de signature.
</p>

<p>
Les paquets des versions mises à jour de shim de Debian sont maintenant
disponibles au moyen de buster-proposed-updates pour la version stable
Debian 10 (<q>Buster</q>) et seront inclus dans la version
intermédiaire 10.5 à venir. De nouveaux paquets sont aussi dans l'archive
Debian pour les versions de développement de Debian (unstable et testing).
</p>

<h2><a name="fwupdate_updates">4. Fwupdate</a></h2>

<p>
Les paquets des versions mises à jour de fwupdate de Debian sont maintenant
disponibles au moyen de buster-proposed-updates pour la version stable
Debian 10 (<q>Buster</q>) et seront inclus dans la version
intermédiaire 10.5 à venir. fwupdate a déjà été retiré d'unstable et de
testing il y a un moment en faveur de fwupd.
</p>

<h2><a name="fwupd_updates">5. Fwupd</a></h2>

<p>
Les paquets des versions mises à jour de fwupd de Debian sont maintenant
disponibles au moyen de buster-proposed-updates pour la version stable
Debian 10 (<q>Buster</q>) et seront inclus dans la version
intermédiaire 10.5 à venir. De nouveaux paquets sont aussi dans l'archive
Debian pour les versions de développement de Debian (unstable et testing).
</p>

<h1><a name="buster_point_release">Version intermédiaire Debian 10.5 (<q>Buster</q>) des médias d'installation et autonomes mis à jour</a></h1>

<p>
Il a été prévu que tous les correctifs décrits ici soient inclus dans la
version intermédiaire Debian 10.5 (<q>Buster</q>), qui doit être publiée le
premier août 2020. Cette version 10.5 devrait donc être un bon choix pour
les utilisateurs qui recherchent des médias d'installation et autonomes.
Les images plus anciennes avec Secure Boot pourront ne plus fonctionner
lorsque les révocations seront déployées.
</p>

<h1><a name="more_info">Plus d'informations</a></h1>

<p>
Beaucoup plus d'informations sur la configuration de l'amorçage de sécurité
de Debian se trouvent dans le wiki de Debian — voir <a
href="https://wiki.debian.org/SecureBoot">https://wiki.debian.org/SecureBoot</a>.</p>

<p>
Parmi les autres ressources sur ce sujet :
</p>

<ul>
  <li><a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">article
      <q>BootHole</q> d'Eclypsium</a> décrivant les faiblesses découvertes</li>
  <li><a href="https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/ADV200011">conseils
      de Microsoft pour corriger les fonctions de sécurité dans GRUB</a></li>
  <li><a href="https://wiki.ubuntu.com/SecurityTeam/KnowledgeBase/GRUB2SecureBootBypass">article
      de la base de connaissances d'Ubuntu</a></li>
  <li><a href="https://access.redhat.com/security/vulnerabilities/grub2bootloader">article
      de Red Hat sur la vulnérabilité</a></li>
  <li><a href="https://www.suse.com/c/suse-addresses-grub2-secure-boot-issue/">article
      de SUSE sur la vulnérabilité</a></li>
</ul>
