#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans qemu-kvm, une solution
complète de virtualisation sur du matériel x86 basée sur Quick Emulator (Qemu).

Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9603">CVE-2016-9603</a>

<p>qemu-kvm construit avec l’émulateur VGA Cirrus CLGD 54xx et la prise en
charge du pilote d’affichage VNC est vulnérable à un problème de dépassement de
tampon basé sur le tas. Il pourrait se produire lorsqu’un client Vnc essaie de
mettre à jour son affichage après qu’une opération vga est réalisée par un
client.</p>

<p>Un utilisateur ou processus privilégié dans un client pourrait utiliser ce
défaut pour planter le processus Qemu aboutissant à un déni de service OU
éventuellement être exploité pour exécuter du code arbitraire sur l’hôte avec
les privilèges du processus de Qemu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7718">CVE-2017-7718</a>

<p>qemu-kvm construit avec la prise en charge de l’émulateur VGA Cirrus CLGD 54xx
est vulnérable à un problème d’accès hors limites. Il pourrait se produire lors
de la copie de données VGA à l’aide des fonctions bitblt cirrus_bitblt_rop_fwd_transp_
ou cirrus_bitblt_rop_fwd_.</p>

<p>Un utilisateur privilégié dans un client pourrait utiliser ce défaut pour
planter le processus de Qemu aboutissant à un déni de service.</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7980">CVE-2017-7980</a>

<p>qemu-kvm construit avec la prise en charge de l’émulateur VGA Cirrus CLGD 54xx
est vulnérable à des problèmes d’accès r/w hors limites. Ils pourraient se
produire lors de la copie de données VGA à l’aide de diverses fonctions bitblt.</p>

<p>Un utilisateur privilégié dans un client pourrait utiliser ce défaut
pour planter le processus de Qemu aboutissant à un déni de service OU
éventuellement à une exécution de code arbitraire sur l’hôte avec les privilèges
du processus de Qemu sur l’hôte.</p></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.1.2+dfsg-6+deb7u21.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu-kvm.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-939.data"
# $Id: $
