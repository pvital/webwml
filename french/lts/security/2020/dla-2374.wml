#use wml::debian::translation-check translation="943fdae171ceaeb4a44d08b77255762821cb5331" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème concernant la divulgation du 
mot de passe dans le composant <tt>gnome-shell</tt> du bureau GNOME.</p>

<p>Dans certaines configurations, lors de la déconnexion d’un compte, la boîte de dialogue de
saisie de mot de passe de connexion pourrait réapparaître avec le mot de
passe visible en texte non dissimulé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17489">CVE-2020-17489</a>

<p>Un problème a été découvert dans certaines configurations de GNOME gnome-shell
jusqu’à la version 3.36.4. Lors de la déconnexion d’un compte, la boîte de dialogue de saisie de mot
de passe de connexion pourrait réapparaître avec le mot de
passe encore lisible. Si l’utilisateur avait décidé d’afficher le mot de passe
de manière non dissimulée lors de la connexion, il est alors visible pendant un
bref moment lors d’une déconnexion. (Si le mot de passe n’était jamais montré
en texte lisible, seule la longueur du mot de passe est révélée.)</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.22.3-3+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gnome-shell.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2374.data"
# $Id: $
