#use wml::debian::translation-check translation="09ac502f97fa281c89d32147020893525d59c157" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Mercurial, un système de
gestion de versions décentralisée, évolutif et facile à utiliser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17458">CVE-2017-17458</a>

<p>Dans Mercurial avant la version 4.4.1, il était possible qu’un dépôt malformé
de manière spéciale pouvait faire que les sous-dépôts de Git exécutent du code
arbitraire sous la forme d’un script .git/hooks/post-update testé dans le dépôt.
Une utilisation habituelle de Mercurial empêche la construction de tels dépôts,
mais ils peuvent être créés par un programme.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13346">CVE-2018-13346</a>

<p>La fonction mpatch_apply dans mpatch.c dans Mercurial avant la version 4.6.1
procédait incorrectement dans le cas où le début du fragment était au-delà la fin
des données originelles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13347">CVE-2018-13347</a>

<p>La fonction mpatch.c dans Mercurial avant la version  4.6.1 gérait
incorrectement l’addition et la soustraction d’entiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13348">CVE-2018-13348</a>

<p>La fonction mpatch_decode dans mpatch.c dans Mercurial avant la version 4.6.1
gérait incorrectement certaines situations où il devrait exister au moins douze
octets résiduels après la position en cours dans les données de correctif, mais
qui n’y étaient pas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000132">CVE-2018-1000132</a>

<p>Les versions 4.5 et antérieures de Mercurial contenaient une vulnérabilité
de contrôle d’accès (CWE-285) dans le serveur de protocole pouvant aboutir
à des accès non autorisés. Cette attaque semble exploitable à l’aide d’une
connectivité réseau. Cette vulnérabilité semble être corrigée dans la
version 4.5.1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3902">CVE-2019-3902</a>

<p>Des liens symboliques et des sous-dépôts pouvaient être utilisés pour mettre
en défaut la logique de vérification de chemin dans Mercurial et pour écrire
des fichiers en dehors de la racine du dépôt.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.0-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mercurial.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mercurial, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mercurial">https://security-tracker.debian.org/tracker/mercurial</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2293.data"
# $Id: $
