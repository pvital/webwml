#use wml::debian::translation-check translation="c8be85c2abbc42079a1c3c735faa7c271ea8ebd5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été découvert dans LinuxTV xawtv avant la version 3.107. La
fonction dev_open() dans v4l-conf.c ne réalise pas suffisamment de vérifications
afin d’empêcher un appelant non privilégié du programme d’ouvrir des chemins non
prévus du système de fichiers. Cela permet à un attaquant local avec un accès au
programme v4l-conf setuid-root de tester l’existence de fichiers arbitraires et
de déclencher un « open » sur des fichiers arbitraires avec le mode O_RDWR.
Pour réaliser cela, des composants de chemins relatifs doivent être ajoutés au
chemin du périphérique, comme le montre une commande
v4l-conf -c /dev/../root/.bash_history.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 3.103-3+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xawtv.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2246.data"
# $Id: $
