#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème de régression a été résolu dans poppler, la bibliothèque partagée
de rendu de PDF, introduit avec la version 0.26.5-2+deb8u5.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16646">CVE-2018-16646</a>

<p>Dans Poppler 0.68.0, la fonction Parser::getObj() dans Parser.cc peut
provoquer une récursion infinie à l'aide d'un fichier contrefait. Un attaquant
distant peut exploiter cela pour une attaque par déni de service.</p>

<p>La solution précédente dans Debian LTS corrige le problème ci-dessus dans
XRef.cc, les correctifs ont été obtenus à partir d’une requête de fusion (n° 67)
sur la plateforme Git de développement de l’amont. Malheureusement, cette
requête à été refusée par l’amont et une autre requête de fusion (n° 91) a été
appliquée à la place. Le correctif apparait maintenant dans le fichier
Parser.cc.</p>

<p>Cette version de poppler fournit désormais l’ensemble des modifications qui
est promu par les développeurs amont de poppler (MR n° 91) et abandonne les
correctifs de MR n° 67.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 0.26.5-2+deb8u6.</p>


<p>Nous vous recommandons de mettre à jour vos paquets poppler.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1562-2.data"
# $Id: $
