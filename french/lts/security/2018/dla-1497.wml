#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvés dans qemu, un émulateur rapide de
processeur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8666">CVE-2015-8666</a>

<p>Dépassement de tampon basé sur le tas dans QEMU lorsqu'il est construit avec
l’émulateur de système PC basé sur la puce Q35.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2198">CVE-2016-2198</a>

<p>Déréférencement de pointeur NULL dans ehci_caps_write dans la prise en charge
de l’USB EHCI pouvant aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6833">CVE-2016-6833</a>

<p>Utilisation après libération lors de l’écriture dans le périphérique vmxnet3
pouvant être utilisée pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6835">CVE-2016-6835</a>

<p>Dépassement de tampon dans vmxnet_tx_pkt_parse_headers() dans le périphérique
vmxnet3 pouvant aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8576">CVE-2016-8576</a>

<p>Vulnérabilité de boucle infinie dans xhci_ring_fetch dans la prise en charge
de l’USB xHCI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8667">CVE-2016-8667</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8669">CVE-2016-8669</a>

<p>Erreur de division par zéro dans set_next_tick dans l’émulateur de jeu de
puces JAZZ RC4030, et dans serial_update_parameters de quelques périphériques
série, pouvant aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9602">CVE-2016-9602</a>

<p>Suivi de lien incorrect avec VirtFS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9603">CVE-2016-9603</a>

<p>Dépassement de tampon basé sur le tas à l’aide d’une connexion vnc dans la
prise en charge de l’émulateur VGA pour Cirrus CLGD 54xx.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9776">CVE-2016-9776</a>

<p>Boucle infinie lors de la réception de données dans l’émulation du contrôleur
Fast Ethernet ColdFire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9907">CVE-2016-9907</a>

<p>Fuite de mémoire dans la prise en charge du redirecteur USB usb-guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9911">CVE-2016-9911</a>

<p>Fuite de mémoire dans ehci_init_transfer dans la prise en charge de EHCI USB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9914">CVE-2016-9914</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9915">CVE-2016-9915</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9916">CVE-2016-9916</a>

<p>Système de fichiers de Plan 9 (9pfs) : ajout d’une opération de nettoyage
dans FileOperations, dans la gestion du dorsal et dans le pilote du mandataire de
dorsal.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9921">CVE-2016-9921</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9922">CVE-2016-9922</a>

<p>Division par zéro dans cirrus_do_copy dans la prise en charge de l’émulateur
VGA de Cirrus CLGD 54xx.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10155">CVE-2016-10155</a>

<p>Fuite de mémoire dans hw/watchdog/wdt_i6300esb.c permettant aux utilisateurs
privilégié de l’OS invité de provoquer un déni de service à l'aide d'un grand
nombre d’opérations de débranchement de périphérique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2615">CVE-2017-2615</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-2620">CVE-2017-2620</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-18030">CVE-2017-18030</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-5683">CVE-2018-5683</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-7718">CVE-2017-7718</a>

<p>Problèmes d’accès hors limites dans la prise en charge de l’émulateur
VGA de Cirrus CLGD 54xx pouvant aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5525">CVE-2017-5525</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-5526">CVE-2017-5526</a>

<p>Problèmes de fuite de mémoire dans l’émulation des périphériques ac97 et
es1370.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5579">CVE-2017-5579</a>

<p>Fuite de mémoire dans l’émulation de l’UART 16550A.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5667">CVE-2017-5667</a>

<p>Accès hors limites durant le transfert multi-bloc dans la prise en charge de l’émulation SDHCI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">CVE-2017-5715</a>

<p>Mitigations de la vulnérabilité Spectre v2. Pour plus d’informations, consulter
<a href="https://www.qemu.org/2018/01/04/spectre/">https://www.qemu.org/2018/01/04/spectre/</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5856">CVE-2017-5856</a>

<p>Fuite de mémoire dans la prise en charge de l’émulation de l’adaptateur de
bus hôte SAS 8708EM2 MegaRAID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5973">CVE-2017-5973</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-5987">CVE-2017-5987</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-6505">CVE-2017-6505</a>

<p>Problèmes de boucle infinie dans l’USB xHCI, dans le registre de transfert
de mode et l’USB ohci_service_ed_list</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7377">CVE-2017-7377</a>

<p>9pfs : fuite de mémoire de l’hôte à l’aide de v9fs_create.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7493">CVE-2017-7493</a>

<p>Problèmes de contrôle d’accès dans le partage de répertoire hôte à l’aide de
prise en charge de 9pfs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7980">CVE-2017-7980</a>

<p>Dépassement de tampon basé sur le tas dans le périphérique VGA de Cirrus
pouvant permettre à des utilisateurs de l’OS invité local d’exécuter du code
arbitraire ou causer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8086">CVE-2017-8086</a>

<p>9pfs : fuite de mémoire de l’hôte à l’aide de v9pfs_list_xattr.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8112">CVE-2017-8112</a>

<p>Boucle infinie dans l’émulation PVSCSI VMWare.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8309">CVE-2017-8309</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-8379">CVE-2017-8379</a>

<p>Problèmes de fuite de mémoire de l’hôte à l’aide du tampon de capture audio
et des gestionnaires d’évènements de saisie de clavier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9330">CVE-2017-9330</a>

<p>Boucle infinie due à une valeur de retour incorrecte dans l’USB OHCI pouvant
aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9373">CVE-2017-9373</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-9374">CVE-2017-9374</a>

<p>Fuite de mémoire de l’hôte durant le débranchement à chaud dans l’IDE AHCI et
les périphériques USB émulés, pouvant aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9503">CVE-2017-9503</a>

<p>Déréférencement de pointeur NULL durant le traitement de commande megasas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10806">CVE-2017-10806</a>

<p>Dépassement de tampon de pile dans le redirecteur USB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10911">CVE-2017-10911</a>

<p>Disque Xen pouvant laisser fuir des données de pile à l’aide de structure de
réponse (response ring).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11434">CVE-2017-11434</a>

<p>Lecture hors limites lors de l’analyse des options Slirp/DHCP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14167">CVE-2017-14167</a>

<p>Accès hors limites durant le traitement d’en-têtes multiboot pouvant aboutir à
l'exécution de code arbitraire</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15038">CVE-2017-15038</a>

<p>9pfs : divulgation d'informations lors de la lecture des attributs étendus.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15289">CVE-2017-15289</a>

<p>Problème d’accès en écriture hors limites dans l’adaptateur graphique de
Cirrus pouvant aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16845">CVE-2017-16845</a>

<p>Fuite d'informations dans la prise en charge de l’émulation de la souris et
du clavier PS/2 pouvant être exploitée pendant la migration d’instance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18043">CVE-2017-18043</a>

<p>Dépassement d’entier dans la macro ROUND_UP (n, d) pouvant aboutir à un déni
de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7550">CVE-2018-7550</a>

<p>Traitement incorrect de la mémoire lors du multiboot pouvant aboutir à
l’exécution de code arbitraire.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:2.1+dfsg-12+deb8u7.</p>
<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1497.data"
# $Id: $
