#use wml::debian::translation-check translation="39e804ed0c941d47bc81e4223d116c7d17d3ef4d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de proftpd-dfsg publiée sous DLA-1753-1 causait une régression
lors de l’utilisation du module sftp. La connexion sur le serveur sftp était
impossible lorsque l’option SFTPPAMEngine était activée (n° 926719).</p>

<p>Cette mise à jour revient à la version 1.3.5 de l’amont puisque même la
dernière publication 1.3.6 de l’amont est toujours affectée par différents bogues
relatifs à sftp (n° 927270). Tous les correctifs pour la fuite de mémoire ont été
rétroportés séparément.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 1.3.5e+r1.3.5-2+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets proftpd-dfsg.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1753-2.data"
# $Id: $
