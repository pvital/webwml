#use wml::debian::translation-check translation="f2c06ef0a874aeb21cd9ebce2275905d6ef34ac0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans advancecomp, une collection
d’utilitaires de recompression.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1056">CVE-2018-1056</a>

<p>Joonun Jang a découvert que l’outil advzip était prédisposé à un dépassement
de tampon basé sur le tas. Cela pourrait permettre à un attaquant de provoquer
un déni de service (plantage d'application) ou avoir un impact non spécifié à
l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9210">CVE-2019-9210</a>

<p>La fonction png_compress dans pngex.cc dans advpng est sujette à un
dépassement d'entier lorsqu’elle fait face à une taille de PNG non valable. Cela
pourrait aboutir à un autre dépassement de tampon basé sur le tas.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1.19-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets advancecomp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1702.data"
# $Id: $
