#use wml::debian::translation-check translation="8e9e707e4372fb507841c6ea89cdabc72bef104a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l’encodeur et décodeur
faad2 (Freeware Advanced Audio Coder).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19502">CVE-2018-19502</a>

<p>Dépassement de tampon basé sur le tas dans la fonction excluded_channels
(libfaad/syntax.c). Cette vulnérabilité peut permettre à des attaquants distants
de provoquer un déni de service à l’aide de données AAC MPEG contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20196">CVE-2018-20196</a>

<p>Dépassement de tampon basé sur le tas dans la fonction calculate_gain
(libfaad/br_hfadj.c). Cette vulnérabilité peut permettre à des attaquants
distants de provoquer un déni de service ou tout autre impact non précisé à
l’aide de données AAC MPEG contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20199">CVE-2018-20199</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-20360">CVE-2018-20360</a>

<p>Déréférencement de pointeur NULL dans la fonction ifilter_bank
(libfaad/filtbank.c). Cette vulnérabilité peut permettre à des attaquants
distants de provoquer un déni de service à l’aide de données AAC
MPEG contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6956">CVE-2019-6956</a>

<p>Dépassement global de tampon dans la fonction ps_mix_phase (libfaad/ps_dec.c).
Cette vulnérabilité peut permettre à des attaquants distants de provoquer un
déni de service ou tout autre impact non précisé à l’aide de données
AAC MPEG contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15296">CVE-2019-15296</a>

<p>Dépassement de tampon dans la fonction faad_resetbits (libfaad/bits.c). Cette
vulnérabilité peut permettre à des attaquants distants de provoquer un déni de
service à l’aide de données AAC MPEG contrefaites.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.7-8+deb8u3.</p>
<p>Nous vous recommandons de mettre à jour vos paquets faad2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1899.data"
# $Id: $
