<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Max Kellermann reported a NULL pointer dereference flaw in libapreq2, a
generic Apache request library, allowing a remote attacker to cause a
denial of service against an application using the library (application
crash) if an invalid nested <q>multipart</q> body is processed.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 2.13-7~deb9u1.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 2.13-7~deb10u1.</p>

<p>We recommend that you upgrade your libapreq2 packages.</p>

<p>For the detailed security status of libapreq2 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libapreq2">https://security-tracker.debian.org/tracker/libapreq2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4541.data"
# $Id: $
