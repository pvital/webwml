<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two security issues have been discovered in the PostgreSQL database
system, which could result in privilege escalation, denial of service or
memory disclosure.</p>

<p>For additional information please refer to the upstream announcement at
<a href="https://www.postgresql.org/about/news/1960/">\
https://www.postgresql.org/about/news/1960/</a></p>

<p>For the stable distribution (buster), these problems have been fixed in
version 11.5-1+deb10u1.</p>

<p>We recommend that you upgrade your postgresql-11 packages.</p>

<p>For the detailed security status of postgresql-11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postgresql-11">\
https://security-tracker.debian.org/tracker/postgresql-11</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4493.data"
# $Id: $
