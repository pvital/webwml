<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a heap-based buffer overread
vulnerability in expat, an XML parsing library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15903">CVE-2019-15903</a>

    <p>In libexpat before 2.2.8, crafted XML input could fool the parser into
    changing from DTD parsing to document parsing too early; a consecutive call
    to <tt>XML_GetCurrentLineNumber</tt> (or
    <tt>XML_GetCurrentColumnNumber</tt>) then resulted in a heap-based buffer
    over-read.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.0-6+deb8u6.</p>

<p>We recommend that you upgrade your expat packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1912.data"
# $Id: $
