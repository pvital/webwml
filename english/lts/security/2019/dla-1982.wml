<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in OpenAFS, a
distributed file system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18601">CVE-2019-18601</a>

    <p>OpenAFS is prone to denial of service from unserialized data access
    because remote attackers can make a series of VOTE_Debug RPC calls
    to crash a database server within the SVOTE_Debug RPC handler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18602">CVE-2019-18602</a>

    <p>OpenAFS is prone to an information disclosure vulnerability because
    uninitialized scalars are sent over the network to a peer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18603">CVE-2019-18603</a>

    <p>OpenAFS is prone to information leakage upon certain error
    conditions because uninitialized RPC output variables are sent over
    the network to a peer.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.6.9-2+deb8u9.</p>

<p>We recommend that you upgrade your openafs packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1982.data"
# $Id: $
