<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that libssh, a tiny C SSH library, does not sufficiently
sanitize path parameters provided to the server, allowing an attacker
with only SCP file access to execute arbitrary commands on the server.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.6.3-4+deb8u4.</p>

<p>We recommend that you upgrade your libssh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2038.data"
# $Id: $
