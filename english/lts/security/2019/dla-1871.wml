<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several minor issues have been fixed in vim, a highly configurable
text editor.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11109">CVE-2017-11109</a>

    <p>Vim allows attackers to cause a denial of service (invalid free)
    or possibly have unspecified other impact via a crafted source
    (aka -S) file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17087">CVE-2017-17087</a>

    <p>Vim sets the group ownership of a .swp file to the editor's
    primary group (which may be different from the group ownership of
    the original file), which allows local users to obtain sensitive
    information by leveraging an applicable group membership.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12735">CVE-2019-12735</a>

    <p>Vim did not restrict the `:source!` command when executed in a
    sandbox.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:7.4.488-7+deb8u4.</p>

<p>We recommend that you upgrade your vim packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1871.data"
# $Id: $
