<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in spamassassin, a Perl-based spam
filter using text analysis.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11805">CVE-2018-11805</a>

    <p>Malicious rule or configuration files, possibly downloaded from an
    updates server, could execute arbitrary commands under multiple
    scenarios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12420">CVE-2019-12420</a>

    <p>Specially crafted mulitpart messages can cause spamassassin to use
    excessive resources, resulting in a denial of service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.4.2-0+deb8u2.</p>

<p>We recommend that you upgrade your spamassassin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2037.data"
# $Id: $
