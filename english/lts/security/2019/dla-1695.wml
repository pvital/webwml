<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in SoX (Sound eXchange),
a sound processing program:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15370">CVE-2017-15370</a>

    <p>The ImaAdpcmReadBlock function (src/wav.c) is affected by a heap buffer
    overflow. This vulnerability might be leveraged by remote attackers
    using a crafted WAV file to cause denial of service (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15372">CVE-2017-15372</a>

    <p>The lsx_ms_adpcm_block_expand_i function (adpcm.c) is affected by a
    stack based buffer overflow. This vulnerability might be leveraged by
    remote attackers using a crafted audio file to cause denial of service
    (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15642">CVE-2017-15642</a>

    <p>The lsx_aiffstartread function (aiff.c) is affected by a use-after-free
    vulnerability. This flaw might be leveraged by remote attackers using a
    crafted AIFF file to cause denial of service (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18189">CVE-2017-18189</a>

    <p>The startread function (xa.c) is affected by a null pointer dereference
    vulnerability. This flaw might be leveraged by remote attackers using a
    crafted Maxis XA audio file to cause denial of service (application
    crash).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
14.4.1-5+deb8u2.</p>

<p>We recommend that you upgrade your sox packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1695.data"
# $Id: $
