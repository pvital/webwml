<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a CRLF injection attack in the Go
programming language runtime library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9741">CVE-2019-9741</a>

    <p>Passing \r\n to http.NewRequest could allow execution of arbitrary HTTP headers or Redis commands.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:1.3.3-1+deb8u2.</p>

<p>We recommend that you upgrade your golang packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1749.data"
# $Id: $
