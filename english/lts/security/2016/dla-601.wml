<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The quagga package installs world readable sensitive files in /etc/quagga,
and might be subject to denial of service because of lacking packet size
checks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4036">CVE-2016-4036</a>

  <p>The quagga package before 0.99.23-2.6.1 uses weak permissions for
  /etc/quagga, which allows local users to obtain sensitive information
  by reading files in the directory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4049">CVE-2016-4049</a>

  <p>The bgp_dump_routes_func function in bgpd/bgp_dump.c in Quagga does
  not perform size checks when dumping data, which might allow remote
  attackers to cause a denial of service (assertion failure and daemon
  crash) via a large BGP packet.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.99.22.4-1+wheezy3.</p>

<p>We recommend that you upgrade your quagga packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-601.data"
# $Id: $
