<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a denial of service attack in the SQLite
database, often embedded into other programs and servers.</p>

<p>In the event of a semantic error in an aggregate query, SQLite did not
return early from the "resetAccumulator()" function which would lead to a crash
via a segmentation fault.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11655">CVE-2020-11655</a>

    <p>SQLite through 3.31.1 allows attackers to cause a denial of service
    (segmentation fault) via a malformed window-function query because the
    AggInfo object's initialization is mishandled.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.8.7.1-1+deb8u5.</p>

<p>We recommend that you upgrade your sqlite3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2203.data"
# $Id: $
