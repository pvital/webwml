<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in netty-3.9, a Java NIO
client/server socket framework.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16869">CVE-2019-16869</a>

    <p>Netty before 4.1.42.Final mishandles whitespace before the colon in
    HTTP headers (such as a "Transfer-Encoding : chunked" line), which
    leads to HTTP request smuggling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20444">CVE-2019-20444</a>

    <p>HttpObjectDecoder.java in Netty before 4.1.44 allows an HTTP header
    that lacks a colon, which might be interpreted as a separate header
    with an incorrect syntax, or might be interpreted as an "invalid
    fold."</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20445">CVE-2019-20445</a>

    <p>HttpObjectDecoder.java in Netty before 4.1.44 allows a Content-Length
    header to be accompanied by a second Content-Length header, or by a
    Transfer-Encoding header.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.9.9.Final-1+deb9u1.</p>

<p>We recommend that you upgrade your netty-3.9 packages.</p>

<p>For the detailed security status of netty-3.9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/netty-3.9">https://security-tracker.debian.org/tracker/netty-3.9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2365.data"
# $Id: $
