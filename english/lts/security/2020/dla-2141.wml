<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The following CVEs were reported against yubikey-val.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10184">CVE-2020-10184</a>

    <p>The verify endpoint in YubiKey Validation Server before 2.40 does
    not check the length of SQL queries, which allows remote attackers
    to cause a denial of service, aka SQL injection.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10185">CVE-2020-10185</a>

    <p>The sync endpoint in YubiKey Validation Server before 2.40 allows
    remote attackers to replay an OTP.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.27-1+deb8u1.</p>

<p>We recommend that you upgrade your yubikey-val packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2141.data"
# $Id: $
