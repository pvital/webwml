<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Meh Chang discovered a buffer overflow flaw in a utility function used
in the SMTP listener of Exim, a mail transport agent. A remote attacker
can take advantage of this flaw to cause a denial of service, or
potentially the execution of arbitrary code via a specially crafted
message.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
4.80-7+deb7u6.</p>

<p>We recommend that you upgrade your exim4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1274.data"
# $Id: $
