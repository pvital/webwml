<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The git-annex package was found to have multiple vulnerabilities when
operating on untrusted data that could lead to arbitrary command
execution and encrypted data exfiltration.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12976">CVE-2017-12976</a>

    <p>git-annex before 6.20170818 allows remote attackers to execute
    arbitrary commands via an ssh URL with an initial dash character
    in the hostname, as demonstrated by an ssh://-eProxyCommand= URL,
    a related issue to <a href="https://security-tracker.debian.org/tracker/CVE-2017-9800">CVE-2017-9800</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-12836">CVE-2017-12836</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-1000116">CVE-2017-1000116</a>, and
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-1000117">CVE-2017-1000117</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10857">CVE-2018-10857</a>

    <p>git-annex is vulnerable to a private data exposure and
    exfiltration attack. It could expose the content of files located
    outside the git-annex repository, or content from a private web
    server on localhost or the LAN.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10859">CVE-2018-10859</a>

    <p>git-annex is vulnerable to an Information Exposure when decrypting
    files. A malicious server for a special remote could trick
    git-annex into decrypting a file that was encrypted to the user's
    gpg key. This attack could be used to expose encrypted data that
    was never stored in git-annex</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.20141125+oops-1+deb8u2.</p>

<p>We recommend that you upgrade your git-annex packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1495.data"
# $Id: $
