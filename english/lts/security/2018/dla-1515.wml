<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Luis Merino, Markus Vervier and Eric Sesterhenn discovered that missing
input sanitising in the Hylafax fax software could potentially result in
the execution of arbitrary code via a malformed fax message.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3:6.0.6-6+deb8u1.</p>

<p>We recommend that you upgrade your hylafax packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1515.data"
# $Id: $
