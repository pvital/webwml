<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in rtmpdump and the librtmp
library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8270">CVE-2015-8270</a>

    <p>A bug in AMF3ReadString in librtmp can cause a denial of service via
    application crash to librtmp users that talk to a malicious server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8271">CVE-2015-8271</a>

    <p>The AMF3_Decode function in librtmp doesn't properly validate its
    input, which can lead to arbitrary code execution when talking
    to a malicious attacker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8272">CVE-2015-8272</a>

    <p>A bug in rtmpsrv can lead to a crash when talking to a malicious
    client.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.4+20111222.git4e06e21-1+deb7u1.</p>

<p>We recommend that you upgrade your rtmpdump packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-917.data"
# $Id: $
