<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the interpreter for the
Ruby language. The Common Vulnerabilities and Exposures project
identifies the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17405">CVE-2017-17405</a>

    <p>A command injection vulnerability in Net::FTP might allow a
    malicious FTP server the execution of arbitrary commands.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17790">CVE-2017-17790</a>

    <p>A command injection vulnerability in lib/resolv.rb's lazy_initialze
    might allow a command injection attack. However untrusted input to
    this function is rather unlikely.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.8.7.358-7.1+deb7u5.</p>

<p>We recommend that you upgrade your ruby1.8 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1222.data"
# $Id: $
