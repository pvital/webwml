<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A heap based buffer overflow has been discovered in the tiff2pdf
utility, part of the Tag Image File Format (TIFF) library.</p>

<p>A PlanarConfig=Contig image can cause an out-of-bounds write (related to
the ZIPDecode function). A crafted input may lead to a remote denial of
service attack or an arbitrary code execution attack.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.9.6-11+deb7u8.</p>

<p>We recommend that you upgrade your tiff3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1094.data"
# $Id: $
