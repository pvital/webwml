<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Denial of Service due to Exhaustion of Packet-ID counter</p>

<p>An authenticated client can cause the server's the packet-id counter to
roll over, which would lead the server process to hit an ASSERT() and
stop running. To make the server hit the ASSERT(), the client must first
cause the server to send it 2^32 packets (at least 196GB). 
 
For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.2.1-8+deb7u4.</p>

<p>We recommend that you upgrade your openvpn packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-944.data"
# $Id: $
