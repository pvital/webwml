<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Brandon Perry discovered that openexr, a high dynamic-range (HDR) image
library, was affected by an integer overflow vulnerability and missing
boundary checks that would allow a remote attacker to cause a denial of
service (application crash) via specially crafted image files.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.6.1-6+deb7u1.</p>

<p>We recommend that you upgrade your openexr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1083.data"
# $Id: $
