#use wml::debian::template title="Checklist do(a) candidato(a)"
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
#use wml::debian::translation-check translation="517d7a4f999561b8f28e207214a40990fdc3da49"

<p>As informações nesta página, embora públicas, são de interesse
principalmente dos(as) futuros(as) desenvolvedores(as) Debian.</p>

<h3>0. Contribuindo</h3>

<p>Antes de decidir se candidatar, você já deve estar contribuindo com o Debian
fornecendo pacotes, traduções, documentação ou qualquer outra atividade.
Normalmente você já está registrado como
<a href="https://wiki.debian.org/DebianMaintainer">mantenedor(a) Debian</a> e
fazendo upload de seus pacotes há seis meses.</p>

<p>O processo atual de registro para novos(as) membros(as) é dividido em 4
partes:</p>

<h3>1. Candidatura e defesa</h3>
<p> Normalmente o processo é iniciado com uma candidatura através da
<a href="https://nm.debian.org/public/newnm">interface web para candidatura
de novos(as) membros(as)</a>.
<br>
Após esta etapa, o(a) candidato(a) é conhecido(a) pelo sistema e a secretaria
de novos(as) membros(as) do Debian é o contato primário para perguntas sobre a
candidatura. A secretaria gerencia ou supervisiona todas as outras etapas e
tenta ajudar a resolver todos os problemas que aparecem.
</p>

<p>Depois que a candidatura é enviada para o sistema, um(a) desenvolvedor(a)
oficial do Debian que trabalhou com o(a) candidato(a) faz a sua defesa. Isso é
feito com um e-mail assinado, contendo um texto breve sobre o(a) candidato(a).
Normalmente este e-mail cobre tópicos como os antecedentes do(a) candidato(a),
o que ele(a) tem feito pelo Debian e algumas citações sobre planos futuros.</p>

<p>Em seguida, a secretaria analisa o que o(a) candidato(a) já fez no projeto.
Para que o processo de NM seja o mais eficiente, o(a) candidato(a) já
deve ter contribuído significativamente para o Debian. Isso pode ser feito
através de empacotamento, documentação, garantia de qualidade, ...</p>

<p><a href="./nm-step1">Mais ...</a></p>

<h3>2. Verificação do gestor(a) de candidatura (AM)</h3>
<p>Assim que um(a) gestor(a) de candidatura estiver disponível, a secretaria
atribui um(a) ao(a) candidato(a). A tarefa do(a) gestor(a) de candidatura é
coletar as informações necessárias para servir de base para a decisão do(a)
gestor(a) de contas Debian. Isso é dividido em 4 partes:</p>

<p>Para manter a rede de confiança que conecta todos(as) os(as)
desenvolvedores(as) Debian, o(a) candidato(a) precisam se identificar
fornecendo uma chave OpenPGP que esteja assinada por pelo menos dois(duas)
desenvolvedores(as) oficiais. Para garantir ainda mais sua identidade, é
altamente recomendável ter assinaturas de outras pessoas (que não precisam ser
DDs, mas devem estar bem conectadas no geral na rede de confiança).</p>

<p><a href="./nm-step2">More ...</a></p>

<h4>Verificação de filosofia e procedimentos</h4>
<p>Como o Debian é conhecido por sua forte base ética e filosófica, o(a)
candidato(a) precisa explicar sua própria visão sobre o tópico de Software
Livre. O Debian também desenvolveu procedimentos padrão bastante complicados
para lidar com os problemas de trabalhar com um grande grupo, então o(a)
candidato(a) precisa mostrar que conhece esses procedimentos e são capazes
de aplicá-los a situações concretas.</p>

<p><a href="./nm-step3">Mais ...</a></p>

<h4>Verificação de tarefas e habilidades</h4>
<p>Para garantir a qualidade geral da distribuição Debian, o(a) candidato(a)
precism mostrar que sabe suas tarefas na área em que planeja trabalhar
(documentação e internacionalização ou manutenção de pacotes).
#Temos mais áreas, tenho certeza disso - mas não consigo me lembrar delas agora: FIXME
Eles(as) também precisam mostrar suas habilidades enviando exemplos de seus
trabalhos e respondendo a algumas perguntas sobre eles.</p>

<p><a href="./nm-step4">Mais ...</a></p>

<h4>Recomendação</h4>
<p>Se o(a) gestor(a) de candidatura estiver satisfeito com o desempenho do(a)
candidato(a), um relatório da candidatura é preparado e enviado para a
secretaria e para os(as) gestores(as) de contas Debian.</p>

<p><a href="./nm-step5">Mais ...</a></p>

<h3>3. Verificação da secretaria</h3>
<p>O relatório da candidatura é verificado por um(a) membro(a) da secretaria do
Debian quanto a problemas formais. Se eles forem graves, o relatório é rejeitado
e o(a) gestor(a) de candidatura precisará corrigir o problema. Se houver apenas
pequenos erros, eles serão relatados ao(a) candidato(a) e ao(a) gestor(a) de
candidatura.</p>

<p><a href="./nm-step6">Mais ...</a></p>

<h3>4. Verificação do(a) gestor(a) de contas Debian e criação da conta</h3>
<p>Nesta última etapa do processo, o relatório da candidatura é avaliado.
Se necessário, outras verificações são feitas pelo(a) gestor(a) de contas
ou é solicitado a um(a) gestor(a) de candidatura realizar uma nova verificação.
Às vezes é necessário um telefonema para concluir a candidatura.</p>

<p>Se todos os possíveis problemas estão resolvidos, os(as) gestores(as) de
contas Debian atribuem uma conta ao(a) candidato(a) e adicionam sua chave
OpenPGP ao chaveiro Debian.</p>

<p><a href="./nm-step7">Mais ...</a></p>
