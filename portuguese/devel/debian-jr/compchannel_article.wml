#use wml::debian::template title="Artigo da Computer Channel sobre o Debian Jr."
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

<P>
A tradução que segue, feita pelo Projeto Debian Jr. deste <a
href="http://computerchannel.de/trends/linux_fk/linux_fk_1.phtml">artigo
original</a> (em alemão) por Susanne Reininger, está postado aqui com
permissão do G+J Computer Channel GmbH.
</P>

<H2>Um Linux para os jovens</H2>

<P>
<i>O sistema operacional livre Linux deve ganhar
espaço nos quartos das crianças - certamente isto é o que os fundadores
de três projetos desejam. Mas saber se o Linux se encaixa desta forma simples
em espaços infantis, como está nas mentes desses(as) comprometidos(as)
desenvolvedores(as), é questionável.</i>
</P>

<P>
por Susanne Reininger
</P>

<P>
      "Papai, este jogo Linux também está disponível para Windows 95 -- ele
ficaria melhor lá, não?", Victoria, de nove anos de idade, pergunta
nervosamente ao pai. Isto machuca o desenvolvedor Debian Ben Armstrong de
duas formas: como um comprometido defensor da ideia de código aberto, ele é um
dos fundadores do "Projeto Debian Jr.", que objetiva deixar o sistema
operacional Debian, baseado em Linux, mais amigável para crianças. Além disso,
ele quer passar suas convicções de "software livre para todos(as)" para seus(suas)
filhos(as) porque ele classifica as alternativas como "muito restritivas e
simplesmente inaceitáveis". "Não importa se eu gosto ou não, o Windows é
muito mais fácil para crianças do que o Linux", Armstrong teve que admitir.
"Por que o Linux não pode ser mais divertido para crianças? Eu quero
desenvolver um desktop para crianças onde elas possam iniciar
brinquedos, jogos e softwares educacionais com poucos cliques do mouse",
descreve Armstrong em sua visão do Projeto Debian Jr.
</P>

<P>
&nbsp;&nbsp;&nbsp;&nbsp;O projeto ainda não é visível ao público e está
situado na "fase interna" na qual Armstrong troca ideias e
dicas com outros(as) desenvolvedores(as) Debian em seu tempo livre, sobre como
ficariam as funções de desktop e aplicações baseadas em Linux que são adequadas
para crianças poderem olhar em uma
<a href="https://lists.debian.org/debian-jr/">lista de discussão</a>
{nota do(a) tradutor(a): isto não é mais verdade, existe
<a href="$(HOME)/devel/debian-jr/">um site web
oficial</a>}. "Isto não é sobre reinventar uma interface de usuário(a), mas
sobre selecionar aplicações que achamos que as crianças lidariam melhor", diz
Armstrong.
</P>

<P>
&nbsp;&nbsp;&nbsp;&nbsp;A questão central é: "O que exatamente faz
com que computadores sejam atrativos para crianças?". Portanto, você deve
manter seus olhos e ouvidos abertos, e olhar discretamente sobre os ombros
das crianças quando elas sentam na frente do computador.
Sem este foco, você se perde em questões abstratas", acredita o desenvolvedor
Debian. Não somente se debate a organização do desktop, mas também
aplicativos testados com crianças de forma que elas não sejam limitadas, mas
encorajadas a encontrar suas próprias soluções - "mesmo que quebre
de vez em quando".
</P>

<P>
&nbsp;&nbsp;&nbsp;&nbsp;Além do Projeto Debian Jr., existem mais duas
iniciativas de desenvolvedores(as) que querem entusiasmar as crianças a
usarem o Linux de forma lúdica, um sistema operacional que é difícil para
muitos(as) usuários(as) comuns entenderem.
Um delas é a "Linux for Kids" ("Linux para Crianças", em português), que
lançou seu próprio <a href="http://www.linuxforkids.com">site web</a>.
Ele quer dar publicidade para o sistema operacional aberto como uma
plataforma de aprendizado e software "edutainment" (educação e
entretenimento). "Nosso público-alvo são crianças com menos de dez anos
de idade" diz Chris Ellec, o operador do site web. Mas até agora,
o Linux for Kids serve somente como um fórum de software para pais/mães e
professores(as) que procuram por programas para os jovens em casa ou para
ensino. "Nós esperamos que também nos tornemos um ponto de contato para
crianças, em breve", diz Ellec.
</P>

<P>
&nbsp;&nbsp;&nbsp;&nbsp;Chris Ellec e alguns(as) amigos(as) queriam adaptar
alguns 486s antigos para colocarem em uma escola de ensino fundamental.
Através de sua longa e duradoura experiência com Linux, ele instalou o
sistema operacional livre nessas "boas e velhas caixas". Mas aí ele
descobriu que mal existiam programas educacionais e jogos. Então ele
reuniu softwares existentes da net e encorajou alguns(as) amigos(as)
programadores(as) a desenvolver aplicativos apropriados. Assim, passo por
passo, o fórum de software cresceu de cinco para mais de 60 programas. Sob
sete categorias, jogos como "Tuxracer", uma corrida de esqui em 3D, o
simulador de cidades "Lincity", um tradutor multilinguagem de japonês
e programas educacionais podem ser baixados. Em fevereiro, saiu em CD-ROM uma
coleção com os melhores programas, cuja receita se volta para a
operação do site. Um sistema de críticas e comentários dos(as) usuários(as)
deve ser adicionado logo. Os fundadores do "Linux for Kids" trabalham
juntos ao <a href="http://www.seul.org/edu/">SEUL/Edu (Simple End User
Linux)</a> (Linux simples de usuário(a) final, em português), um grupo de
discussão de pais/mães, professores(as) e estudantes que querem
implementar o Linux como um auxílio de estudo.
</P>

<P>
&nbsp;&nbsp;&nbsp;&nbsp;O Tux, mascote do Linux, já se tornou um
brinquedo fofinho nos quartos das crianças - agora ele somente precisa
conseguir pular para a mesa do computador dos pequenos.
</P>

<P>
&copy; por G+J Computer Channel GmbH
</P>
