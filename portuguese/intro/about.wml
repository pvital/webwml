#use wml::debian::template title="Sobre o Debian"
#use wml::debian::translation-check translation="849f83139b69ccd125dd30434842680c0deb8971"
#include "$(ENGLISHDIR)/releases/info"

<ul class="toc">
<li><a href="#what">Afinal de contas, o que é o Debian?</a></li>
<li><a href="#free">É tudo livre?</a></li>
<li><a href="#CD">Você diz livre, mas os CDs e a banda de rede custam dinheiro!</a></li>
<li><a href="#disbelief">A maioria dos softwares custa mais de 100 dólares.
            Como vocês conseguem distribuí-lo assim?</a></li>
<li><a href="#hardware">Qual hardware é suportado?</a></li>
<li><a href="#info">Antes de decidir, preciso de mais informações.</a></li>
<li><a href="#why">Ainda não estou convencido. Quais são alguns prós e contras do Debian?</a></li>
<li><a href="#install">Como eu consigo o Debian?</a></li>
<li><a href="#support">Eu não consigo configurar tudo sozinho.
    Onde eu consigo ajuda para o Debian?</a></li>
<li><a href="#who">Afinal de contas, quem são vocês?</a></li>
<li><a href="#users">Quem usa Debian?</a></li>
<li><a href="#history">Como tudo isso começou?</a></li>
</ul>

<h2><a name="what">Afinal de contas, o que é o Debian?</a></h2>

<p>O <a href="$(HOME)/">Projeto Debian</a> é uma associação de
indivíduos que têm como causa comum criar um sistema operacional
<a href="free">livre</a>. O sistema operacional que criamos é chamado
<strong>Debian</strong>.

<p>Um sistema operacional é o conjunto de programas básicos e utilitários
que fazem seu computador funcionar.
No núcleo do sistema operacional está o kernel.
O kernel é o programa mais fundamental no computador e faz todas
as operações mais básicas, permitindo que você execute outros programas.

<p>Os sistemas Debian atualmente usam o kernel
<a href="https://www.kernel.org/">Linux</a> ou o kernel
<a href="https://www.freebsd.org/">FreeBSD</a>.
O Linux é parte de um software criado inicialmente por
<a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a>
com a ajuda de milhares de programadores(as) espalhados por todo o mundo.
O FreeBSD é um sistema operacional que inclui um kernel e outros softwares.

<p>No entanto, existe um esforço para fornecer o Debian para outros
kernels, principalmente para o
<a href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>.
O Hurd é um conjunto de servidores que rodam em cima de um microkernel (como o
Mach) a fim de implementar diferentes características. O Hurd é um software
livre produzido pelo <a href="https://www.gnu.org/">projeto GNU</a>.

<p>Uma grande parte das ferramentas básicas que formam o sistema operacional
originam-se do <a href="https://www.gnu.org/">projeto GNU</a>; daí os nomes:
GNU/Linux, GNU/kFreeBSD e GNU/Hurd.
Essas ferramentas também são livres.

<p>Claro que são os aplicativos o que as pessoas querem: programas para
ajudá-las a realizar suas tarefas, desde a edição de documentos até a
administração de negócios, passando por jogos e desenvolvimento de mais
softwares. O Debian vem com mais <packages_in_stable> <A href="$(DISTRIB)/packages">pacotes</a>
(softwares pré-compilados e empacotados em um formato amigável, o que faz com
que sejam de fácil instalação em sua máquina), um gerenciador de pacotes (APT) e
outros utilitários que tornam possível gerenciar milhares de pacotes em milhares
de computadores de maneira tão fácil quanto instalar um único aplicativo. Todos
são <a href="free">livres</a>.

<p>Funciona como uma torre: na base dela está o kernel. Sobre ele estão
todas as ferramentas básicas e, acima delas, estão todos os outros softwares que
você executa em seu computador.
No topo da torre está o Debian &mdash; cuidadosamente organizado e ajustado, de
modo que tudo funcione bem enquanto todos esses componentes trabalham em
conjunto.

<h2>É tudo <a href="free" name="free">livre?</a></h2>

<p>Você deve estar pensando: por que as pessoas gastam tantas horas de seu tempo
para escrever software, empacotá-lo cuidadosamente e então <EM>distribuí-lo</EM>?
As respostas são tão variadas quanto as pessoas que contribuem.
Algumas pessoas gostam de ajudar os outros.
Muitas escrevem programas para aprender mais sobre computadores.
Mais e mais pessoas estão procurando maneiras de evitar o alto preço de
software.
Uma multidão contribui como forma de agradecimento por todos os excelentes
softwares livres que receberam dos outros.
Muitas na academia criam softwares livres a fim de que os resultados de
suas pesquisas tenham um uso mais amplo.
Empresas ajudam a manter o software livre para que possam opinar na forma como
ele é desenvolvido -- não há maneira
mais rápida de conseguir obter um novo recurso do que implementá-lo você mesmo(a)!
Claro, muitos de nós apenas acha tudo isso muito divertido!

<p>O Debian é tão comprometido com o software livre que decidimos que
seria útil que esse compromisso fosse formalizado em um documento escrito.
Assim nasceu o nosso <a href="$(HOME)/social_contract">Contrato Social</a>.

<p>Apesar do Debian acreditar no software livre, há casos em que as pessoas
querem ou precisam colocar software não livre em suas máquinas. Sempre que
possível, o Debian dará suporte a esses(as) usuários(as). Há até mesmo um número
crescente de pacotes cujo único trabalho é instalar software não livre em
sistemas Debian.

<h2><a name="CD">Você diz livre, mas os CDs e a banda de rede custam dinheiro!</a></h2>

<p> Você pode estar se perguntando: se o software é livre ("free", em inglês,
também significa "grátis"), então por que eu tenho que pagar a um vendedor por
um CD ou a um servidor de internet pela conexão para fazer o download do mesmo?

<p>Quando você compra um CD, está pagando pelo tempo de alguém, pelo
investimento para criar os discos e pelo risco (no caso de todos os CDs
não serem vendidos). Em outras palavras, você está pagando por uma mídia
física usada para entregar o software, não pelo software em si.

<p>Quando usamos a palavra "free" (livre), estamos nos referindo
à <strong>liberdade</strong> de software, não a custo zero. Você pode
ler mais sobre <a href="free">o que consideramos "software livre"</a> e
<a href="https://www.gnu.org/philosophy/free-sw">o que a Free Software
Foundation diz</a> sobre o assunto.

<h2><a name="disbelief">A maioria dos softwares custa mais de 100
dólares. Como vocês conseguem distribuí-lo assim?</a></h2>

<p>Uma pergunta melhor é: como as empresas de software conseguem cobrar tanto?
Desenvolver software não é como fazer um carro. Uma vez feita uma cópia do seu
software, os custos de produção para se fazer um milhão de cópias a mais são
muito pequenos (essa é uma razão pela qual a Microsoft tem bilhões de dólares no
banco).

<p>Olhe de outro modo: se você tivesse um suprimento infinito de areia no seu
quintal, você iria querer doar essa areia. Mesmo assim, seria idiota pagar para
um caminhão levá-la para outras pessoas. Você faria com que essas pessoas
viessem buscar sua areia elas mesmas (equivalente a baixar da internet), ou elas
podem pagar a alguém para entregar na porta da casa delas (equivalente a comprar um CD).
Isso é exatamente o que o Debian faz e essa é a razão dos CDs/DVDs serem tão
baratos (por volta de R$10,00 o DVD).

<p>O Debian não ganha dinheiro vendendo CDs. Mesmo assim, é necessário
dinheiro para pagar despesas como registro de domínio e hardware. Dessa forma,
pedimos que você compre as mídias de um dos <a
href="../CD/vendors/">vendedores de CDs</a> que <a
href="$(HOME)/donations">doam</a> uma parte do valor da venda ao projeto Debian.

<h2><a name="hardware">Qual hardware é suportado?</a></h2>

<p>O Debian pode ser executado em quase todos os computadores pessoais,
incluindo os mais antigos. Cada novo lançamento do Debian geralmente suporta
um número maior de arquiteturas. Para consultar uma lista completa das
arquiteturas atualmente suportadas, leia a <a
href="../releases/stable/">documentação da versão estável (<q>stable</q>)</a>.

<p>Quase todo hardware comum é suportado. Se você quer ter certeza de que todos
os dispositivos conectados à sua máquina são suportados, confira a
<a href="https://wiki.debian.org/InstallingDebianOn/">página wiki do DebianOn</a>.

<p>Existem algumas empresas que dificultam o suporte ao não liberar
especificações de seu hardware. Isso significa que você pode não conseguir usar
o hardware deles com GNU/Linux. Algumas empresas fornecem drivers não livres e
esse é um problema, pois a empresa poderia mais tarde sair do negócio ou
interromper o suporte para o hardware que você possui. Recomendamos que você
apenas adquira hardwares de fabricantes que fornecem drivers
<a href="free">livres</a> para seus produtos.

<h2><a name="info">Antes de decidir, preciso de mais informações.</a></h2>

<p>Você pode dar uma olhada no nosso <a href="$(DOC)/manuals/debian-faq/">FAQ</a>.

<h2><a name="why">Ainda não estou convencido. Quais são alguns prós e contras do Debian?</a></h2>

<p>Não nos dê ouvidos, experimente o Debian você mesmo(a). Já que o espaço em disco
tem se tornado cada vez mais barato, você provavelmente pode dispor de 2GB de
espaço em disco. Se você não quer ou não precisa de uma interface gráfica, 600MB
são suficientes. O Debian pode facilmente ser instalado nesse espaço extra e
coexistir com seu sistema operacional existente. Se você eventualmente precisar
de mais espaço, pode simplesmente remover um dos seus sistemas operacionais (e
depois de ver o poder de um sistema Debian, temos certeza de que você não
removerá o Debian).

<p>Experimentar um novo sistema operacional tomará uma boa parte do seu
valioso tempo, por isso é compreensível que você possa ter suas reservas. Por
essa razão, compilamos uma lista de <a href="why_debian">prós e contras do
Debian</a>. Isso deve ajudá-lo a decidir se vale a pena ou não.
Esperamos que você aprecie nossa honestidade e franqueza.

<h2><a name="install">Como consigo o Debian?</a></h2>

<p>O método de instalação do Debian mais popular é através de CDs, os quais você
pode adquirir com um dos nossos vários vendedores de CDs. Caso você disponha de
bom acesso à Internet, é possível baixar e instalar o Debian pela Internet.</p>

<p>Por favor, consulte <a href="../distrib/">nossa página sobre como obter
o Debian</a> para maiores informações.</p>

<h2><a name="support">Eu não consigo configurar tudo sozinho. Como consigo
ajuda para o Debian?</a></h2>

<p>Você pode obter ajuda lendo a documentação que está disponível tanto nas
páginas do site web quanto nos pacotes que você pode instalar em seu sistema. Você
também pode entrar em contato conosco através das listas de discussão ou usando
o IRC. É possível ainda contratar um consultor para fazer o serviço.</p>

<p>Por favor, veja nossa <a href="../doc/">documentação</a> e páginas de
<a href="../support">suporte</a> para maiores informações.</p>

<h2><a name="who">Afinal de contas, quem são vocês?</a></h2>

<p>O Debian é produzido por aproximadamente mil desenvolvedores(as)
ativos(as) espalhados(as)
<a href="$(DEVEL)/developers.loc">pelo mundo</a> que são voluntários(as)
em seu tempo livre.
Poucos(as) desenvolvedores(as) se conhecem pessoalmente. A comunicação é
realizada principalmente através de e-mail (listas de discussão
em lists.debian.org) e IRC (canal #debian-br em português, ou #debian em inglês,
no irc.debian.org).</p>

<p>O Projeto Debian tem uma <a href="organization">estrutura organizada</a>
cuidadosamente. Para maiores informações sobre como o Debian é internamente,
sinta-se livre para navegar pelo
<a href="$(DEVEL)/">Canto dos(as) desenvolvedores(as)</a>.</p>

<p>A lista completa de membros(as) oficiais do Debian pode ser encontrada em
<a href="https://nm.debian.org/members">nm.debian.org</a>, onde a comunidade é
gerenciada. Uma lista mais ampla de colaboradores(as) do Debian pode ser
encontrada em
<a href="https://contributors.debian.org">contributors.debian.org</a>.</p>

<h2><a name="users">Quem usa o Debian?</a></h2>

<p>Embora não exista uma estatística precisa (já que o Debian não exige que seus
usuários(as) se registrem), existem fortes evidências de que o Debian é
utilizado por um amplo número de grandes e pequenas organizações, bem como por
milhares de indivíduos. Veja nossa página
<a href="../users/">Quem está usando o Debian?</a> para uma lista de
organizações importantes que nos enviaram uma breve descrição de como e porque
estão usando o Debian.

<h2><a name="history">Como tudo isso começou?</a></h2>

<p>O Debian foi criado em agosto de 1993 por Ian Murdock, como uma nova
distribuição que seria feita abertamente, no espírito do Linux e do projeto GNU.
O Debian foi concebido para ser cuidadosamente e conscientemente organizado, e
ser mantido e suportado com cuidado similar. Tudo começou como um pequeno grupo
de hackers de Software Livre que, gradativamente, cresceu e tornou-se uma grande
e organizada comunidade de desenvolvedores(as) e usuários(as). Veja
<a href="$(DOC)/manuals/project-history/">a história detalhada</a>.

<p>Já que muitas pessoas perguntaram, Debian é pronunciado /&#712;de.bi.&#601;n/.
O nome vem do nome de seu criador, Ian Murdock, e sua ex-esposa, Debra.
