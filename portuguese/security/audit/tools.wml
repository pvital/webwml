#use wml::debian::template title="Ferramentas de auditoria de segurança"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="be191e77facf8c0d489cfd320232517e5233a3e2"

<p>Existem diversos pacotes disponíveis dentro do repositório do Debian
que são projetados para ajudar em auditorias de código-fonte. Dentre eles:</p>

<ul>
<li><a href="https://packages.debian.org/flawfinder">Flawfinder</a>
<ul>
<li><a href="examples/flawfinder">Exemplo de utilização do flawfinder</a></li>
</ul></li>
<li><a href="http://archive.debian.net/woody/its4">ITS4</a>
<ul>
<li>Não existe exemplo para o ITS4 já que ele foi removido da distribuição
instável (unstable).</li>
</ul></li>
<li><a href="https://packages.debian.org/rats">RATS</a>
<ul>
<li><a href="examples/RATS">Exemplo de utilização do RATS</a></li>
</ul></li>
<li><a href="https://packages.debian.org/pscan">pscan</a>
<ul>
<li><a href="examples/pscan">Exemplo de utilização do pscan</a></li>
</ul></li>
</ul>

<p>Além disso, note que existem outras ferramentas específicas para
determinados conjuntos de vulnerabilidades de segurança que talvez não
estejam empacotadas para o Debian ainda, mas podem ser úteis para um(a)
auditor(a). Dentre elas:</p>

<ul>
<li>Ferramentas específicas para bugs XSS:
<ul>
<li><a href="http://freecode.com/projects/xsslint/">Xsslint</a></li>
<li><a href="http://www.devitry.com/screamingCSS.html">ScreamingCSS</a></li>
</ul></li>
<li>Ferramentas para teste de navegadores web:
<ul>
<li><a href="http://www.securityfocus.com/archive/1/378632">MangleMe</a></li>
</ul></li>
</ul>

<p>Nenhuma dessas ferramentas é perfeita e somente podem ser usadas como
diretrizes para estudo posterior, mas dado que são muito simples de usar,
é válido gastar um tempo para tentá-las.</p>

<p>Cada uma das ferramentas tem diferentes pontos fortes e fracos, então
recomenda-se usar mais de uma.</p>


<h2>Flawfinder</h2>

<p>O flawfinder é uma ferramenta Python que é projetada para analisar
código-fonte C e C++ e procurar por possíveis falhas de segurança.</p>

<p>Quando executada contra um diretório contendo código-fonte, ela retornará
um relatório dos possíveis problemas detectados, classificados por risco
(onde <i>risco</i> é um inteiro 1&ndash;5). Para ignorar riscos menores, é
possível dizer ao programa para não reportar falhas abaixo de um determinado
nível de risco. Por padrão a saída aparecerá em texto puro, mas um relatório
HTML também está disponível.</p>

<p>O programa trabalha escaneando o código e procurando pelo uso de
funções que estão contidas dentro do seu banco de dados de funções, que
são comumente usadas indevidamente.</p>

<p>Para ajudar na leitura do relatório, é possível fazer com que o relatório
de saída contenha a linha que possua a função que está sendo usada; isto pode
ser útil para detectar imediatamente um problema ou rejeitá-lo da mesma
forma.</p>

<p>Você pode ver um exemplo de como o flawfinder é usado, e sua saída,
na <a href="examples/">seção de exemplos de auditoria</a>.</p>

<h2>ITS4</h2>

<p>O ITS4 é uma ferramenta contida na seção non-free do repositório Debian e
somente está disponível para a versão <q>woody</q>.</p>

<p>O ITS4 pode ser usado para escanear tanto código C quanto C++, procurando
por possíveis brechas de segurança, muito parecido com o flawfinder.</p>

<p>A saída que ele produz tenta ser inteligente, ignorando alguns casos
nos quais as chamadas para funções perigosas foram feitas
cuidadosamente.</p>


<h2>RATS</h2>

<p>O RATS é uma ferramenta similar às listadas acima, com a exceção
de que ela vem com suporte para uma gama muito maior de linguagens.
Atualmente, ela tem suporte para C, C++, Perl, PHP e Python.</p>

<p>A ferramenta usa um arquivo XML simples para ler suas vulnerabilidades,
de modo que é uma das ferramentas disponíveis mais fáceis de modificar.
Novas funções podem ser adicionadas facilmente para cada linguagem
suportada.</p>

<p>Você pode ver um exemplo de como o RATS é usado, e sua saída, na
<a href="examples/">seção de exemplos de auditoria</a>.</p>

<h2>pscan</h2>

<p>O pscan difere das ferramentas descritas anteriormente
porque não é um scanner de propósito geral. Ao contrário, ela é
um programa especificamente voltado para detecção de bugs de formato de
string.</p>

<p>A ferramenta tentará encontrar possíveis problemas com o uso de
funções variadas dentro do código-fonte C e C++, tais como
<tt>printf</tt>, <tt>fprintf</tt> e <tt>syslog</tt>.</p>

<p>Bugs de formato de string são fáceis de detectar e corrigir; embora
eles sejam a mais recente classe de ataques de software, a maioria
deles provavelmente já foi encontrada e reparada.</p>

<p>Você pode ver um exemplo de como o pscan é usado, e sua saída, na
<a href="examples/">seção de exemplos de auditoria</a>.</p>


<h2>Entendendo a saída do escaneamento</h2>

<p>Cada uma das ferramentas gerais de escaneamento incluirá uma saída
descrevendo a falha detectada e possivelmente dará sugestões sobre como o
código pode ser corrigido.</p>

<p>Por exemplo, a saída a seguir foi retirada do RATS e
descreve os perigos da <tt>getenv</tt>:</p>

<p>&quot;Variáveis de ambiente são entradas altamente não confiáveis. Elas podem
ser de qualquer tamanho e podem conter qualquer dado. Não faça qualquer
suposição em relação ao conteúdo ou tamanho. Se possível evite usá-las a
todo custo, e se necessário, higienize-as e restrinja-as para tamanhos
razoáveis&quot;.</p>

<p>Se precisar de conselhos adicionais sobre como corrigir uma falha que
foi reportada, você deve estudar um livro de programação segura, como o
documento (em inglês) <a href="http://www.dwheeler.com/secure-programs/">HOWTO
programação segura para Linux e Unix</a> do David A. Wheeler.</p>

<p>(Lembre-se de que ao reportar problemas de segurança, uma correção (patch)
que conserte essa brecha é altamente apreciada)</p>

<p>Discussões relativas ao fechamento de partes de código particularmente
problemáticos podem ser feitas na
<a href="https://lists.debian.org/debian-security/">lista de discussão
debian-security</a>; como esta é uma lista de discussão com arquivo público,
tenha cuidado em não deixar óbvio qual programa contém a falha.</p>