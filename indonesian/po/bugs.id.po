msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-08-26 20:50+0700\n"
"Last-Translator: Izharul Haq <atoz.chevara@yahoo.com>\n"
"Language-Team: L10N Debian Indonesian <debian-l10n-indonesian@lists.debian."
"org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "pada paket"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "tagged"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "dengan severity"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "pada paket sumber"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "pada paket yang dikelola oleh"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "dikirim oleh"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "dimiliki oleh"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "dengan status"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "dengan email dari"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "kutu terbaru"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "dengan mengandung subyek"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "dengan kondisi tertunda"

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "dengan mengandung pengirim"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "dengan mengandung dialihkan"

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "dengan mengandung pemilik"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "dengan paket"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "normal"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "oldview"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "raw"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "usia"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Repeat Merged"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Reverse Bugs"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "Reverse Pending"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Reverse Severity"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr "No Bugs which affect packages"

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "None"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "testing"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "oldstable"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "stable"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "experimental"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "unstable"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "Unarchived"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "Archived"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "Archived and Unarchived"
