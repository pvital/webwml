#use wml::debian::template title="Berater" NOCOMMENTS="yes" GENTIME="yes"
#use wml::debian::translation-check translation="edbd4a65aeffde3b466134d488992d26b78b73eb"
#  Translator: Thimo Neubauer <thimo@debian.org>
# Updated: Holger Wansing <linux@wansing-online.de>, 2013, 2019.

<p>Debian ist freie Software und bietet Hilfe über
<a href="$(HOME)/MailingLists/">Mailinglisten</a> an. Einige
Menschen haben jedoch entweder keine Zeit dafür oder spezielle
Wünsche, so dass sie bereit sind, jemanden dafür zu
bezahlen, einen Debian-Rechner zu warten oder weitere
Funktionalität zu schreiben. Es folgt eine Liste von Leuten, die
zumindest einen Teil ihres Einkommens durch <strong>bezahlte</strong>
Unterstützung von Debian verdienen.</p>

<p>Die Namen sind nach Ländern sortiert, innerhalb eines Landes
aber einfach in der Reihenfolge der Anmeldung sortiert. Wenn Ihr Land
nicht aufgeführt wird, schauen Sie auch in den
Nachbarländern, weil manche Berater auch international tätig
sind. Debian listet die Berater nur als Service an die Benutzer auf und
übernimmt keine Garantien für die aufgelisteten Menschen
oder Firmen. Bitte beachten Sie die <a href="info">Berater-Informationsseite</a> für
unsere Richtlinien über das Hinzufügen oder Aktualisieren eines Eintrags.</p>

<p>Bitte beachten Sie, dass einige der Berater einen Teil ihres
Honorars an Debian spenden. Es liegt aber im Ermessen des Beraters
oder der Firma, dies zu tun. Sie wissen jetzt aber darum und werden
hoffentlich Ihre Wahl danach richten ...</p>

<p><strong>
Die Liste wird angeboten wie sie ist, ohne jegliche Garantie oder
Befürwortung durch das Debian-Projekt. Die Einträge stehen unter der
alleinigen Verantwortung des jeweiligen Beraters.
</strong></p>

<p>Es gibt weitere Listen von Beratern für spezielle Anwendungsfelder von Debian:</p>
<ul>
<li><a href="https://wiki.debian.org/DebianEdu/Help/ProfessionalHelp">Debian Edu</a>:
für die Verwendung von Debian in Schulen, Universitäten und anderen Bildungseinrichtungen.
<li><a href="https://wiki.debian.org/LTS/Funding">Debian LTS</a>:
für die Langzeit-Sicherheits-Unterstützung von Debian.
</ul>

<hrline>

<h2>Länder, in denen es einen aufgelisteten Debian-Berater gibt:</h2>

#include "../../english/consultants/consultant.data"

<hrline>

# left for backwards compatibility - in case someone still links to #policy
# which is fairly unlikely, but hey
<h2><a name="policy"></a>Richtlinien für Debians Beraterseite</h2>

<p>Bitte lesen Sie die <a href="info">Berater-Informationsseite</a> für unsere
   Richtlinien über das Hinzufügen oder Aktualisieren eines Eintrags.</p>
