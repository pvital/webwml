#use wml::debian::translation-check translation="73b976c71b8b4c13c331a478bd9111aa6f64627e" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В cacti, системе мониторинга сервера, были обнаружены многочисленные проблемы,
потенциально приводящие к выполнению SQL-кода или раскрытию информации
аутентифицированным пользователям.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16723">CVE-2019-16723</a>

    <p>Аутентифицированные пользователи могут обходить проверки авторизации для просмотра
    графиков, отправляя запросы с изменёнными параметрами local_graph_id.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17357">CVE-2019-17357</a>

    <p>Интерфейс администрирования графиков недостаточно очищает параметр
    template_id, что потенциально приводит к SQL-инъекции. Эта уязвимость
    может использоваться аутентифицированными злоумышленниками для выполнения
    неавторизованного выполнения SQL-кода в базе данных.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17358">CVE-2019-17358</a>

    <p>Функция sanitize_unserialize_selected_items (lib/functions.php) недостаточно
    очищает передаваемые пользователем данные до их десериализации, что потенциально
    приводит к небезопасной десериализации пользовательских данных. Эта уязвимость
    может использоваться аутентифицированными злоумышленниками для влияния на логику
    выполнения программы или для вызова повреждения содержимого памяти.</p></li>

</ul>

<p>В предыдущем стабильном выпуске (stretch) эти проблемы были исправлены
в версии 0.8.8h+ds1-10+deb9u1. Заметьте, что stretch подвержен только
<a href="https://security-tracker.debian.org/tracker/CVE-2018-17358">CVE-2018-17358</a>.</p>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 1.2.2+ds1-2+deb10u2.</p>

<p>Рекомендуется обновить пакеты cacti.</p>

<p>С подробным статусом поддержки безопасности cacti можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/cacti">\
https://security-tracker.debian.org/tracker/cacti</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4604.data"
