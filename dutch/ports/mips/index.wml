#use wml::debian::template title="Debian voor MIPS"
#use wml::debian::translation-check translation="9b957cdd51147e7f52a88511c30631446b341174"

#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc

<toc-display/>

<toc-add-entry name="about">Situatieschets</toc-add-entry>
<p>De versie van Debian voor MIPS bestaat eigenlijk uit twee versies,
<em>debian-mips</em> en <em>debian-mipsel</em>. De binaire bestanden
verschillen inzake <a href="http://foldoc.org/endian">endianness</a>
(bytevolgorde). MIPS CPU's zijn in staat om met beide bytevolgordes
om te gaan, maar aangezien dit normaal niet te wijzigen valt in software,
hebben we beide architecturen nodig. SGI-machines werken in <a
href="http://foldoc.org/big-endian">big-endian</a>-modus (debian-mips),
terwijl de Loongson 3 machines in
<a href="http://foldoc.org/little-endian">little-endian</a>-modus
(debian-mipsel) functioneren. Sommige borden, zoals Broadcom's BCM91250A
evaluatiebord (ook gekend als SWARM) kan in beide modi functioneren, welke
geselecteerd kunnen worden met een schakelaar op het bord. Sommige op Cavium
Octeon gebaseerde machines kunnen tussen beide modi schakelen in de bootloader.</p>

<p>Omdat de meeste MIPS-machines een 64-bits CPU hebben, wordt momenteel
gewerkt aan een <em>debian-mips64el</em>-versie welke mogelijk met Debian
GNU/Linux 9 uitgebracht wordt.</p>

<toc-add-entry name="status">Huidige toestand</toc-add-entry>
<p>Debian GNU/Linux <current_release_jessie> ondersteunt de volgende machines:</p>

<ul>

<li>SGI Indy met R4x00 en R5000 CPUs, en Indigo2 met R4400 CPU (IP22).</li>

<li>SGI O2 met R5000, R5200 en RM7000 CPU (IP32).</li>

<li>Broadcom BCM91250A (SWARM) evaluatiebord (big- en little-endian).</li>

<li>MIPS Malta-borden (big- en little-endian, 32 en 64-bits).</li>

<li>Loongson 2e en 2f machines, inclusief de Yeelong laptop (little-endian).</li>

<li>Loongson 3 machines (little-endian).</li>

<li>Cavium Octeon (big-endian).</li>

</ul>

<p>Behalve op de bovenstaande machines kan men Debian op een heleboel andere
machines gebruiken op voorwaarde dat een niet-Debian kernel wordt gebruikt.
Dit is bijvoorbeeld het geval voor het MIPS Creator Ci20 ontwikkelaarsbord.</p>

<toc-add-entry name="info">Algemene informatie</toc-add-entry>

<p>Raadpleeg de <a href="$(HOME)/releases/stable/mips/release-notes/">\
notities bij de release</a> en de <a href="$(HOME)/releases/stable/mips/">\
installatiehandleiding</a> voor meer informatie.</p>


<toc-add-entry name="availablehw">Voor ontwikkelaars van Debian
beschikbare hardware</toc-add-entry>

<p>Er staan twee machines ter beschikking van ontwikkelaars voor
het geschikt maken van Debian voor MIPS: etler.debian.org (mipsel/mips64el)
en minkus.debian.org (mips). De machines hebben chroot-omgevingen voor
ontwikkelingswerkzaamheden, waartoe u toegang heeft met <em>schroot</em>.
Raadpleeg de <a href = "https://db.debian.org/machines.cgi">machine-database</a>
voor bijkomende informatie over deze machines.</p>


<toc-add-entry name="credits">Medewerkers</toc-add-entry>

<p>Dit is de lijst van mensen die werken aan het geschikt maken van Debian voor
MIPS:</p>

#include "$(ENGLISHDIR)/ports/mips/people.inc"

<toc-add-entry name="contacts">Contact</toc-add-entry>

<h3>Mailinglijsten</h3>

<p>Er zijn enkele mailinglijsten die handelen over Linux/MIPS en in het bijzonder
over Debian op MIPS.</p>

<ul>

<li>debian-mips@lists.debian.org &mdash; Deze lijst gaat over Debian op MIPS.<br />
Teken in via een mail naar <email debian-mips-request@lists.debian.org>.<br />
Het archief bevindt zich op <a href="https://lists.debian.org/debian-mips/">lists.debian.org</a>.</li>

<li>linux-mips@linux-mips.org &mdash; Deze lijst is bedoeld voor discussies over
ondersteuning voor MIPS in de kernel.<br />
Zie de <a href = "https://www.linux-mips.org/wiki/Net_Resources#Mailing_lists">Linux/MIPS</a>-pagina voor informatie over intekenen
op de lijst.</li>

</ul>

<h3>IRC</h3>

<p>U vindt ons op IRC op <em>irc.debian.org</em> op het kanaal
<em>#debian-mips</em>.</p>


<toc-add-entry name="links">Links</toc-add-entry>

<dl>
  <dt>Ontwikkeling van de Linux/MIPS kernel &mdash; Veel gerelateerde informatie over MIPS</dt>
    <dd><a href="https://www.linux-mips.org/">linux-mips.org</a></dd>
  <dt>CPU-leverancier</dt>
    <dd><a href="https://imgtec.com/mips">https://imgtec.com/mips</a></dd>
  <dt>Informatie over SGI-hardware</dt>
    <dd><a href="http://www.sgistuff.net/hardware/">http://www.sgistuff.net/hardware/</a></dd>
  <dt>Debian op SGI Indy</dt>
    <dd><a href="http://www.pvv.org/~pladsen/Indy/HOWTO.html">http://www.pvv.org/~pladsen/Indy/HOWTO.html</a></dd>
  <dt>Debian op SGI Indy</dt>
    <dd><a href="https://nathan.chantrell.net/linux/sgi-indy-and-debian-linux/">https://nathan.chantrell.net/linux/sgi-indy-and-debian-linux/</a></dd>
  <dt>Debian op SGI O2</dt>
    <dd><a href="https://cyrius.com/debian/o2/">http://www.cyrius.com/debian/o2</a></dd>
</dl>


<toc-add-entry name="thanks">Bedanking</toc-add-entry>

<p>De machines voor ontwikkelingswerkzaamheden en de meeste build-servers voor
de architecturen <em>mips</em> en <em>mipsel</em> worden geleverd door
<a href="https://imgtec.com">Imagination Technologies</a>.</p>


<toc-add-entry name="dedication">Opdracht</toc-add-entry>

<p>Thiemo Seufer, de leidinggevende ontwikkelaar voor MIPS in Debian, kwam om
het leven in een auto-ongeluk. We <a href =
"$(HOME)/News/2008/20081229">wijden de release</a> van de
Debian GNU/Linux distributie <q>lenny</q> aan zijn nagedachtenis.</p>

