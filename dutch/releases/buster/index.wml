#use wml::debian::template title="Debian &ldquo;buster&rdquo; release-informatie"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6cb92bf51af80b4a4dc15d92247adf5ba8fe9619"

<if-stable-release release="buster">

<p>Debian <current_release_buster> werd uitgebracht op
 <a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "Debian 10.0 werd oorspronkelijk uitgebracht op
   <:=spokendate('2019-07-06'):>."
/>
De release bevatte verschillende ingrijpende wijzigingen, beschreven in ons <a
href="$(HOME)/News/2019/20190706">persbericht</a> en de <a
href="releasenotes">notities bij de release</a>.</p>

#<p><strong>Debian 10 werd vervangen door
#<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
#Er worden geen beveiligingsupdates meer uitgebracht sinds <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### Deze paragraaf is indicatief; kijk ze na voor publicatie!
#<p><strong>Buster geniet evenwel van langetermijnondersteuning (Long Term Support - LTS) tot
#eind xxxxx 20xx. The LTS is beperkt tot i386, amd64, armel, armhf en arm64.
#Alle andere architecturen worden niet langer ondersteund in buster.
#Raadpleeg voor meer informatie de <a
#href="https://wiki.debian.org/LTS">sectie over LTS op de Wiki van Debian</a>.
#</strong></p>

<p>Raadpleeg de <a href="debian-installer/">installatie-informatie</a>-pagina en de
<a href="installmanual">Installatiehandleiding</a> over het verkrijgen en installeren
van Debian. Zie de instructies in
de <a href="releasenotes">notities bij de release</a> om van een oudere Debian release
op te waarderen.</p>

### Wat volgt activeren wanneer de LTS-periode begint.
#<p>Ondersteunde architecturen tijdens de langetermijnondersteuning:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>De volgende computerarchitecturen worden ondersteund in deze release:</p>
# <p>Ondersteunde computerarchitecturen bij de initiële release van buster:</p> ### Deze regel gebruiken als LTS start, in plaats van die hierboven.


<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release, ondanks dat deze <em>stabiel</em> wordt genoemd. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a> gemaakt
en u kunt ons altijd <a href="reportingbugs">andere problemen rapporteren</a>.</p>

<p>Tot slot, maar niet onbelangrijk, een overzicht van de
<a href="credits">mensen</a> die deze release mogelijk maakten.</p>
</if-stable-release>

<if-stable-release release="stretch">

<p>De codenaam voor de volgende hoofdrelease van Debian na
<a href="../stretch/">stretch</a> is <q>buster</q>.</p>

<p>Deze release startte als een kopie van stretch en is momenteel in een toestand
genaamd <q><a href="$(DOC)/FAQ/ftparchives#testing">testing</a></q>.
Dit betekent dat problemen niet zo erg zouden mogen zijn als die in de
onstabiele of experimentele distributies, omdat pakketten enkel toegelaten
worden na een bepaalde periode en wanneer er geen release-kritieke bugs voor
gerapporteerd zijn.</p>

<p>Merk op dat beveiligingsupdates voor de <q>testing</q>-distributie nog
<strong>niet</strong> beheerd worden door het beveiligingsteam. Dus,
<q>testing</q> krijgt <strong>niet</strong> snel beveiligingsupdates.
# For more information please see the
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">announcement</a>
# of the Testing Security Team.
We raden u aan om voorlopig uw sources.list-regels te veranderen van testing naar
stretch als u beveilingsupdates nodig heeft. Zie ook de <a
href="$(HOME)/security/faq#testing">FAQ van het Beveiligingsteam</a>
betreffende de <q>testing</q>-distributie.</p>

<p>Mogelijk is een <a href="releasenotes">ontwerp beschikbaar van de notities bij de release</a>.
Raadpleeg ook <a href="https://bugs.debian.org/release-notes">de voorgestelde aanvullingen
voor de notities bij de release</a>.</p>

<p>Zie <a href="$(HOME)/devel/debian-installer/">de pagina van het Debian installatiesysteem</a>
voor installatie-images en documentatie over hoe <q>testing</q> te installeren.</p>

<p>Om meer te weten over hoe de <q>testing</q>-distributie werkt, kunt u <a
href="$(HOME)/devel/testing">de ontwikkelaarsinformatie daarover</a> raadplegen.</p>

<p>Mensen vragen dikwijls of er één enkele release-<q>voortgangsmeter</q> is.
Spijtig genoeg is die er niet, maar we kunnen u naar verschillende plaatsen
verwijzen waar beschreven staat wat er moet gebeuren vóór een release kan
worden uitgebracht:</p>

<ul>
  <li><a href="https://release.debian.org/">Algemene status van de release</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Release-kritieke bugs</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Bugs in het basissysteem</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Bugs in standaard- en taakpakketten</a></li>
</ul>

<p>Daarnaast rapporteert de releasemanager regelmatig over de algemene status
op de <a href="https://lists.debian.org/debian-devel-announce/">\
mailinglijst debian-devel-announce</a>.</p>

</if-stable-release>
